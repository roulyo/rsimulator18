#pragma once

#include <forge/core/GameState.h>

#include <gamestates/ScoreInputs.h>

#include <systems/RenderingSystem.h>

//----------------------------------------------------------------------------
class ForgeSample;

//----------------------------------------------------------------------------
class ScoreState : public AbstractGameState
{
public:
    ScoreState(const ForgeSample& _game);
    ~ScoreState();

    void BeginFrame(const long long& _dt) override;
    void EndFrame(const long long& _dt) override;

    void OnPush() override;
    void OnPop() override;

private:
    ScoreInputScheme        m_ScoreInputScheme;
    RenderingSystem         m_RenderingSystem;
};
