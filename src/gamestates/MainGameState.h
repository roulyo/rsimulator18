#pragma once

#include <forge/core/GameState.h>

#include <gamestates/MainGameInputs.h>

#include <systems/RenderingSystem.h>
#include <systems/ClickableWorkerSystem.h>
#include <systems/PlayerControlSystem.h>
#include <systems/PhysicsSystem.h>
#include <systems/StressLevelSystem.h>

//----------------------------------------------------------------------------
class ForgeSample;

//----------------------------------------------------------------------------
class MainGameState : public AbstractGameState
{
public:
    MainGameState(const ForgeSample& _game);
    ~MainGameState();

    void BeginFrame(const long long& _dt) override;
    void EndFrame(const long long& _dt) override;

    void OnPush() override;
    void OnPop() override;

private:
    MainGameInputScheme     m_MainGameInputScheme;

    PlayerControlSystem     m_PlayerControlSystem;
    ClickableWorkerSystem   m_ClickableWorkerSystem;
    RenderingSystem         m_RenderingSystem;
    PhysicsSystem           m_PhysicsSystem;
    StressLevelSystem       m_StressLevelSystem;

};
