#pragma once

#include <forge/system/input/InputMappingScheme.h>
#include <forge/system/input/KeyInputBinder.h>

#include <events/game/RestartGameEvent.h>

//----------------------------------------------------------------------------
 typedef KeyInputBinder<RestartGameEvent, RestartGameEvent> StartKeyBinder;

//----------------------------------------------------------------------------
class StartScreenInputScheme : public InputMappingScheme
{
public:
    StartScreenInputScheme();

private:
    StartKeyBinder m_StartKeyBinder;

};