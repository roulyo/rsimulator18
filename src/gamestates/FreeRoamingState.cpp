#include <gamestates/FreeRoamingState.h>

#include <forge/system/input/InputManager.h>

//----------------------------------------------------------------------------
FreeRoamingState::FreeRoamingState(const AbstractForgeGame& _game)
    : AbstractGameState(_game)
{
    m_Systems.push_back(&m_PlayerControlSystem);
    m_Systems.push_back(&m_PhysicsSystem);
    m_Systems.push_back(&m_RenderingSystem);
    m_Systems.push_back(&m_StressLevelSystem);
}

//----------------------------------------------------------------------------
FreeRoamingState::~FreeRoamingState()
{
}

//----------------------------------------------------------------------------
void FreeRoamingState::BeginFrame(const long long& _dt)
{
}

//----------------------------------------------------------------------------
void FreeRoamingState::EndFrame(const long long& _dt)
{
}

//----------------------------------------------------------------------------
void FreeRoamingState::OnPush()
{
    AbstractGameState::OnPush();

    InputManager::GetInstance().SetInputMapping(&m_FreeRoamingInputScheme);
}

//----------------------------------------------------------------------------
void FreeRoamingState::OnPop()
{
    AbstractGameState::OnPop();

    InputManager::GetInstance().SetInputMapping(nullptr);
}
