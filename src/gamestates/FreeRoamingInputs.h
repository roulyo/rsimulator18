#pragma once

#include <forge/system/input/InputMappingScheme.h>
#include <forge/system/input/KeyInputBinder.h>

#include <events/inputs/MoveEvent.h>

//----------------------------------------------------------------------------
typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::North>,   OrientedMoveEvent<false, MoveDirection::North>> MoveNorthBinder;
typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::East>,    OrientedMoveEvent<false, MoveDirection::East>>  MoveEastBinder;
typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::South>,   OrientedMoveEvent<false, MoveDirection::South>> MoveSouthBinder;
typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::West>,    OrientedMoveEvent<false, MoveDirection::West>>  MoveWestBinder;

//----------------------------------------------------------------------------
class FreeRoamingInputScheme : public InputMappingScheme
{
public:
    FreeRoamingInputScheme();

private:
    MoveNorthBinder m_MoveNorthBinder;
    MoveEastBinder  m_MoveEastBinder;
    MoveSouthBinder m_MoveSouthBinder;
    MoveWestBinder  m_MoveWestBinder;

};