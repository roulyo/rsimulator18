#pragma once

#include <SFML/System/Clock.hpp>
#include <Global.h>
#include <systems/ClickableWorkerSystem.h>
#include <forge/2d/drawable/Text.h>
#include <forge/2d/drawable/Sprite.h>

#include <string>
#include <vector>

//----------------------------------------------------------------------------
class WorkersJobDoneEvent;
class MoneyBonusEvent;
class RestartGameEvent;
class WorkerQuitEvent;
class WorkerTotalStressUpdateEvent;


//----------------------------------------------------------------------------
class GameDirector
{
    struct Feature
    {
        std::string m_Name = "Feature";
        Skill m_Type = Skill::Art;
        float m_Score = 100.0f;
        Sprite::Ptr m_TypeIcon;
    };

public:
    GameDirector();
    ~GameDirector();

    void Init();
    void Update(const long long& _dt);
    
    std::string ComputeCriticsScore();
    std::string ComputeUsersScore();
    std::string GenerateGameName();

    void OnWorkersJobDoneEvent(const WorkersJobDoneEvent& _event);
    void OnMoneyBonusEvent(const MoneyBonusEvent& _event);
    void OnWorkerQuitEvent(const WorkerQuitEvent& _event);
    void OnRestartGameEvent(const RestartGameEvent& _event);
    void OnWorkerTotalStressUpdateEvent(const WorkerTotalStressUpdateEvent& _event);

    bool IsGameOver();
    void Reset();
   
private:
    void InitUI();
    void DisplayGameUI();
    void DisplayScoreUI();

    void DisplaySkillScores();
    void DisplayFeatureList();
    void DisplayMoney();

    void DisplayText(Text& textRef, std::string textStr, float _x = 0.0f, float _y = 0.0f,
                     int _r = 255, int _g = 255, int _b = 255, Text::Style _style = Text::Style::Regular);

    void UpdateFeatureList(const long long& _dt);
    void UpdateMoney(const long long& _dt);
    void CheckForGameOver();

    int GetNbFeaturesDone() const;

    void DebugLog();

    std::vector<Feature>             m_FeatureList;
    sf::Clock                        m_GameTimer;

    float                            m_TotalSalaryToPay;
    float                            m_CurrentWorkersScore[Skill::Skills_Count];
    float                            m_CurrentAvailableMoney;

    Font::Ptr                        m_Font;
    Font::Ptr                        m_SansSerif;
    Text                             m_CountdownStr;
    Text                             m_MoneyStr;
    Text                             m_GameOverStr;
    Text                             m_RestartStr;
    Text                             m_FeatureListStr[Global::NB_FEATURES];
    Text                             m_CurrentWorkersSkillsStr[Skill::Skills_Count];
    Text                             m_GameNameStr;
    Text                             m_MetaCriticScoreStr;
    Text                             m_UserScoreStr;
    Text                             m_MetaSummaryStr;
    Text                             m_UserSummaryStr;

    Sprite::Ptr                      m_ScoreScreenSprite;
    Sprite::Ptr                      m_MetacriticBackground;
    Sprite::Ptr                      m_UserscoreBackground;

    bool                             m_IsGameOver;
    float                            m_TotalStressLevel;
    std::string                      m_GameOverReason;
    std::string                      m_GameName;
    std::string                      m_MetaCriticScore;
    std::string                      m_UserScore;
    std::string                      m_MetaSummary;
    std::string                      m_UserSummary;

    unsigned                         m_MetaCriticScoreInt;
    unsigned                         m_UserScoreInt;
};