#include <gamestates/ScoreInputs.h>

#include <forge/system/input/InputTypes.h>

//----------------------------------------------------------------------------
ScoreInputScheme::ScoreInputScheme()
{
    RegisterKeyInput(Keyboard::Key::Space, &m_RestartKeyBinder);
}