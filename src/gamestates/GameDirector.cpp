#include <gamestates/GameDirector.h>

#include <events/game/WorkersJobDoneEvent.h>
#include <events/game/MoneyBonusEvent.h>
#include <events/game/RestartGameEvent.h>
#include <events/game/WorkerQuitEvent.h>
#include <events/game/WorkerTotalStressUpdateEvent.h>

#include <forge/system/data/DataManager.h>
#include <forge/system/window/WindowManager.h>
#include <forge/audio/AudioManager.h>

#include <data/DataNameIds.h>
#include <data/fonts/FontCatalog.h>
#include <data/audio/SoundCatalog.h>
#include <data/sprites/SpriteCatalog.h>


//----------------------------------------------------------------------------
GameDirector::GameDirector()    
{
    m_FeatureList.reserve(Global::NB_FEATURES);

}

GameDirector::~GameDirector()
{
    WorkersJobDoneEvent::Handlers -= WorkersJobDoneEvent::Handler(this, &GameDirector::OnWorkersJobDoneEvent);
    MoneyBonusEvent::Handlers -= MoneyBonusEvent::Handler(this, &GameDirector::OnMoneyBonusEvent);
    RestartGameEvent::Handlers -= RestartGameEvent::Handler(this, &GameDirector::OnRestartGameEvent);
    WorkerQuitEvent::Handlers -= WorkerQuitEvent::Handler(this, &GameDirector::OnWorkerQuitEvent);
    WorkerTotalStressUpdateEvent::Handlers -= WorkerTotalStressUpdateEvent::Handler(this, &GameDirector::OnWorkerTotalStressUpdateEvent);
    
}

//----------------------------------------------------------------------------
void GameDirector::Init()
{
    MoneyBonusEvent::Handlers += MoneyBonusEvent::Handler(this, &GameDirector::OnMoneyBonusEvent);
    WorkersJobDoneEvent::Handlers += WorkersJobDoneEvent::Handler(this, &GameDirector::OnWorkersJobDoneEvent);
    RestartGameEvent::Handlers += RestartGameEvent::Handler(this, &GameDirector::OnRestartGameEvent);
    WorkerQuitEvent::Handlers += WorkerQuitEvent::Handler(this, &GameDirector::OnWorkerQuitEvent);
    WorkerTotalStressUpdateEvent::Handlers += WorkerTotalStressUpdateEvent::Handler(this, &GameDirector::OnWorkerTotalStressUpdateEvent);

    Global::ResetRandomSeed();

    InitUI();

    Reset();

    //DebugLog();
}

//----------------------------------------------------------------------------
void GameDirector::InitUI()
{
    if (const FontCatalog* catalog = DataManager::GetInstance().GetCatalog<FontCatalog>())
    {
        m_Font = catalog->Get(DataNameIds::Fonts::k_Pixelli);
        m_SansSerif = catalog->Get(DataNameIds::Fonts::k_SansSerif);

        m_CountdownStr.SetFont(m_Font);
        m_CountdownStr.SetZValue(1000);
        m_CountdownStr.SetSize(50);

        m_MoneyStr.SetFont(m_Font);
        m_MoneyStr.SetZValue(1000);
        m_MoneyStr.SetSize(60);

        m_GameOverStr.SetFont(m_SansSerif);
        m_GameOverStr.SetZValue(1000);

        m_RestartStr.SetFont(m_Font);
        m_RestartStr.SetZValue(1000);
        m_RestartStr.SetSize(60);

        m_GameNameStr.SetFont(m_SansSerif);
        m_GameNameStr.SetZValue(1000);
        m_GameNameStr.SetSize(40);

        m_MetaCriticScoreStr.SetFont(m_SansSerif);
        m_MetaCriticScoreStr.SetZValue(1000);
        m_MetaCriticScoreStr.SetSize(60);

        m_UserScoreStr.SetFont(m_SansSerif);
        m_UserScoreStr.SetZValue(1000);
        m_UserScoreStr.SetSize(60);

        m_MetaSummaryStr.SetFont(m_SansSerif);
        m_MetaSummaryStr.SetZValue(1000);
        m_MetaSummaryStr.SetSize(14);

        m_UserSummaryStr.SetFont(m_SansSerif);
        m_UserSummaryStr.SetZValue(1000);
        m_UserSummaryStr.SetSize(14);

        for(Text& featureText : m_FeatureListStr)
        {
            featureText.SetFont(m_Font);
            featureText.SetZValue(1000);
        }

        for (Text& skillText : m_CurrentWorkersSkillsStr)
        {
            skillText.SetFont(m_Font);
            skillText.SetZValue(1000);
            skillText.SetSize(60);
        }    
    }

    if (const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>())
    {
        m_ScoreScreenSprite = catalog->Get(DataNameIds::Sprites::k_ScoreScreen);
        m_ScoreScreenSprite->As<Sprite>()->SetZValue(500);

        m_MetacriticBackground = catalog->Get(DataNameIds::Sprites::k_ScoreBackground);
        m_MetacriticBackground->As<Sprite>()->SetZValue(501);

        m_UserscoreBackground = catalog->Get(DataNameIds::Sprites::k_ScoreBackground);
        m_UserscoreBackground->As<Sprite>()->SetZValue(501);
    }
}

//----------------------------------------------------------------------------
void GameDirector::Update(const long long& _dt)
{
    if (!m_IsGameOver)
    {
        UpdateFeatureList(_dt);
        UpdateMoney(_dt);
        CheckForGameOver();
    }
    if (m_IsGameOver)
    {
        DisplayScoreUI();
    }
    else
    {
        DisplayGameUI();
    }
}

//----------------------------------------------------------------------------
void GameDirector::CheckForGameOver()
{
    const int nbFeaturesDone = GetNbFeaturesDone();

    if (m_CurrentAvailableMoney == 0)
    {
        m_IsGameOver = true;
        m_GameOverReason = "No more money!!! You went over budget!!!";
    }
    else if (Global::GAME_DURATION < m_GameTimer.getElapsedTime().asSeconds())
    {
        m_IsGameOver = true;
        m_GameOverReason = "Time s up! You have to ship the game as is !";
    }
    else if(m_TotalSalaryToPay == 0)
    { 
        m_IsGameOver = true;
        m_GameOverReason = "Everyone left no choice but to ship now...";        
    }
    else if (nbFeaturesDone == Global::NB_FEATURES)
    {
        m_IsGameOver = true;
        m_GameOverReason = "ALL FEATURES DONE - CONGRATULATIONS !";
    }

    if (m_IsGameOver)
    {
        m_GameName += GenerateGameName();
        m_UserScore += ComputeUsersScore();
        m_MetaCriticScore += ComputeCriticsScore();
    }
}

//----------------------------------------------------------------------------
int GameDirector::GetNbFeaturesDone() const
{
    int nbFeaturesDone = 0;
    for (int i = 0; i < Global::NB_FEATURES; ++i)
    {
        if (m_FeatureList[i].m_Score == 0)
        {
            nbFeaturesDone++;
        }
    }
    return nbFeaturesDone;
}

//----------------------------------------------------------------------------
void GameDirector::DisplayGameUI()
{
    int remainingSeconds = static_cast<int>(Global::GAME_DURATION - m_GameTimer.getElapsedTime().asSeconds());
    std::string countdownStr = std::to_string(remainingSeconds);
    if (remainingSeconds > 10)
    {
        DisplayText(m_CountdownStr, countdownStr, 148.0f, -6.0f, 0, 0, 0);
    }
    else
    {
        DisplayText(m_CountdownStr, countdownStr, 148.0f, -6.0f, 255, 0, 0);
    }    

    DisplayMoney();
    DisplaySkillScores();
    DisplayFeatureList();
}

//----------------------------------------------------------------------------
void GameDirector::DisplayFeatureList()
{
    std::string featureStr = "";

    for (int i = 0; i < Global::NB_FEATURES; ++i)
    {
        featureStr.clear();
        featureStr += m_FeatureList[i].m_Name;

        if (m_FeatureList[i].m_Score > 0.0f)
        {
            DisplayText(m_FeatureListStr[i], featureStr, 913.0f, 100.0f + i * 25.0f);
        }
        else
        {
            DisplayText(m_FeatureListStr[i], featureStr, 913.0f, 100.0f + i * 25.0f, 255, 255, 255, Text::Style::StrikeThrough);
            m_FeatureList[i].m_TypeIcon->As<Sprite>()->As<Sprite>()->SetOverlayColor(100, 100, 100);
        }
        m_FeatureList[i].m_TypeIcon->As<Sprite>()->As<Sprite>()->SetScreenCoord(885.0f, 117.0f + i * 25.0f);
        WindowManager::GetInstance().PushToRender(m_FeatureList[i].m_TypeIcon->As<Sprite>()->GetRenderingData());
    }

}

//----------------------------------------------------------------------------
void GameDirector::DisplayMoney()
{
    int money = static_cast<int>(m_CurrentAvailableMoney / 100.0f) * 100;
    int over1000000 = 0;
    int over1000 = 0;
    std::string moneyStr = "";
    if (money > 1000000)
    {
        over1000000 = static_cast<int>(money / 1000000.0f);
        moneyStr += std::to_string(over1000000) + ".";
    }
    if (money > 1000)
    {
        over1000 = static_cast<int>((money - over1000000) / 1000.0f);
        moneyStr += std::to_string(over1000) + ".";
    }
    int under1000 = money - over1000000 * 1000000 - over1000 * 1000;
    moneyStr += ((under1000 == 0) ? "000" : std::to_string(under1000)) + "$";
    DisplayText(m_MoneyStr, moneyStr, 1000.0f, 5.0f, 255, 255, 0);
}

//----------------------------------------------------------------------------
void GameDirector::DisplayScoreUI()
{
    WindowManager::GetInstance().PushToRender(m_ScoreScreenSprite->As<Sprite>()->GetRenderingData());
    
    std::string gameOverMessage = m_GameOverReason;        
    DisplayText(m_GameOverStr, gameOverMessage, 250.0f, 500.0f, 0, 0, 0);
    DisplayText(m_RestartStr, "PRESS SPACE TO RESTART", 250.0f, 600.0f);
    DisplayText(m_GameNameStr, m_GameName, 100.0f, 122.0f, 66, 66, 66);

    std::string metaSummary;
    {
        if (m_MetaCriticScoreInt >= 75)
        {
            m_MetacriticBackground->As<Sprite>()->SetOverlayColor(102, 204, 51);
            
            metaSummary = "Universal acclaim. " + m_GameName + "\n"
                          "is an epic game!\n"
                          "Taking place in an unforgiving heartland,\n"
                          "the game's vast and atmospheric world provides\n"
                          "the foundation for an awesome experience.\n";
            DisplayText(m_MetaSummaryStr, metaSummary, 100.0f, 400.0f, 66, 66, 66);

        }
        else if (m_MetaCriticScoreInt >= 50)
        {
            m_MetacriticBackground->As<Sprite>()->SetOverlayColor(255, 204, 51);

            metaSummary = "Mixed or average. " + m_GameName + "\n"
                          "is a game full of surprise.\n" 
                          "So whether you are going to like this game or not\n"
                          "will depend on the expectations and the ideas\n"
                          "you have around it. \n";
            DisplayText(m_MetaSummaryStr, metaSummary, 100.0f, 400.0f, 66, 66, 66);
        }
        else
        {
            m_MetacriticBackground->As<Sprite>()->SetOverlayColor(255, 0, 0);

            metaSummary = "Generally unfavorable. " + m_GameName + "\n"
                          "is by far the most disappointing game ever made.\n"
                          "DISAPPOINTED!!1";
            DisplayText(m_MetaSummaryStr, metaSummary, 100.0f, 400.0f, 66, 66, 66);
        }
        m_MetacriticBackground->As<Sprite>()->SetScreenCoord(111, 238);

        WindowManager::GetInstance().PushToRender(m_MetacriticBackground->As<Sprite>()->GetRenderingData());
        DisplayText(m_MetaCriticScoreStr, m_MetaCriticScore, 135.0f, 243.0f);
    }    

    std::string userSummary;
    {
        if (m_UserScoreInt >= 75)
        {
            m_UserscoreBackground->As<Sprite>()->SetOverlayColor(102, 204, 51);  

            userSummary = "Congratulations, your company has a good renown,\n"
                          "and all your employees want to continueworking\n"
                          "for you.";
            DisplayText(m_UserSummaryStr, userSummary, 436.0f, 400.0f, 66, 66, 66);
        }
        else if (m_UserScoreInt >= 50)
        {
            m_UserscoreBackground->As<Sprite>()->SetOverlayColor(255, 204, 51);

            userSummary = "World's best OK company.";
            DisplayText(m_UserSummaryStr, userSummary, 436.0f, 400.0f, 66, 66, 6);
        }
        else
        {
            m_UserscoreBackground->As<Sprite>()->SetOverlayColor(255, 0, 0);

            userSummary = "No one wants to work with you ever again.\n"
                          "Kotatsu and PoulisGone are onto you.";
            DisplayText(m_UserSummaryStr, userSummary, 436.0f, 400.0f, 66, 66, 66);
        }
        m_UserscoreBackground->As<Sprite>()->SetScreenCoord(436, 238);

        WindowManager::GetInstance().PushToRender(m_UserscoreBackground->As<Sprite>()->GetRenderingData());
        DisplayText(m_UserScoreStr, m_UserScore, 445.0f, 243.0f);
    }

    DisplayMoney();
    DisplaySkillScores();
    DisplayFeatureList();
}

//----------------------------------------------------------------------------
void GameDirector::DisplaySkillScores()
{
    std::string scoreStr = "";
    for (int i = 0; i < Skill::Skills_Count; ++i)
    {
        scoreStr = std::to_string(static_cast<int>(m_CurrentWorkersScore[i]));
        DisplayText(m_CurrentWorkersSkillsStr[i], scoreStr, 945.0f + i * 135.0f, 618.0f, 255, 255, 255);
    }
}

//----------------------------------------------------------------------------
void GameDirector::DisplayText(Text& textRef, std::string textStr, float _x, float _y,
     int _r /*= 255*/, int _g /*= 255*/, int _b /*= 255*/, Text::Style _style /*= Textt::Regular*/)
{    
    textRef.SetString(textStr);
    textRef.SetFillColor(_r, _g, _b);
    textRef.SetScreenCoord(_x, _y);
    textRef.SetStyle(_style);

    WindowManager::GetInstance().PushToRender(textRef.GetRenderingData());
}

//----------------------------------------------------------------------------
void GameDirector::UpdateFeatureList(const long long& _dt)
{
    for (int i = 0; i < Skill::Skills_Count; ++i)
    {
        for (Feature& feature : m_FeatureList)
        {
            if (feature.m_Score <= 0.0f || feature.m_Type != static_cast<Skill>(i))
                continue;

            float newScore = (feature.m_Score - (m_CurrentWorkersScore[i] * _dt) / 1000.0f);
            if (newScore > 0)
            {
                feature.m_Score = newScore;
            }
            else
            {
                feature.m_Score = 0;

                const SoundCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundCatalog>();

                if (catalog)
                {
                    Sound::Ptr featureSound = catalog->Get(DataNameIds::Sounds::k_Feature);
                    AudioManager::GetInstance().PlaySound(featureSound);
                }
            }
            break;
        }
    }
}

//----------------------------------------------------------------------------
void GameDirector::UpdateMoney(const long long& _dt)
{
    m_CurrentAvailableMoney -= (m_TotalSalaryToPay * _dt) / 1000.0f;
    if (m_CurrentAvailableMoney < 0.0f)
    {
        m_CurrentAvailableMoney = 0.0f;
    }
}

//----------------------------------------------------------------------------
std::string GameDirector::ComputeCriticsScore()
{   
    std::string scoreStr = "";

    float featureDeliveredRatio = static_cast<float>(GetNbFeaturesDone()) / static_cast<float>(Global::NB_FEATURES);
    if (featureDeliveredRatio > 0.85f)
    {
        m_MetaCriticScoreInt = 84 + (rand() % 15);
        scoreStr += std::to_string(m_MetaCriticScoreInt); //"MIGHTY  " + 
    }
    else if (featureDeliveredRatio > 0.75f)
    {
        m_MetaCriticScoreInt = 75 + (rand() % 9);
        scoreStr += std::to_string(m_MetaCriticScoreInt); //"STRONG  " + 
    }
    else if (featureDeliveredRatio > 0.66f)
    {
        m_MetaCriticScoreInt = 66 + (rand() % 9);
        scoreStr += std::to_string(m_MetaCriticScoreInt); //"FAIR  " + 
    }
    else if (featureDeliveredRatio > 0.5f)
    {
        m_MetaCriticScoreInt = 50 + (rand() % 15);
        scoreStr += std::to_string(m_MetaCriticScoreInt); //"FAIR  " + 
    }
    else
    {
        m_MetaCriticScoreInt = 1 + (rand() % 50);
        scoreStr += std::to_string(m_MetaCriticScoreInt); //"WEAK  " + 
    }

    return scoreStr;
}

//----------------------------------------------------------------------------
std::string GameDirector::ComputeUsersScore()
{
    int nbWorkersLeft = static_cast<int>(m_TotalSalaryToPay / Global::WORKER_SALARY);
    int nbQuittedWorkers = Global::NB_STARTING_WORKERS - nbWorkersLeft;
    int averageStressLevel = 0;
    if (nbWorkersLeft > 0)
    {
        averageStressLevel = static_cast<int>(m_TotalStressLevel / nbWorkersLeft);
    }

    float quitScore = (1.0f - static_cast<float>(nbWorkersLeft) / static_cast<float>(Global::NB_STARTING_WORKERS)) * 100.0f;
    float stressScore = (100.0f - (averageStressLevel / 3.0f));
    float totalScore = stressScore - quitScore;
    if (totalScore < 0.0f)
    {
        totalScore = 0.0f;
    }

    std::string usersStr = "";
//     usersStr += "\n Employees left " + std::to_string(nbWorkersLeft) + " out of " + std::to_string(Global::NB_STARTING_WORKERS);
//     usersStr += "\n Average stress " + std::to_string(averageStressLevel);
    m_UserScoreInt = totalScore;
    usersStr += std::to_string(static_cast<int>(totalScore));

    return usersStr;
}

//----------------------------------------------------------------------------
std::string GameDirector::GenerateGameName()
{
    return Global::startGameName[rand() % Global::startGameName.size()] + Global::endGameName[rand() % Global::endGameName.size()] + Global::followGameName[rand() % Global::followGameName.size()];
}

//----------------------------------------------------------------------------
void GameDirector::OnWorkersJobDoneEvent(const WorkersJobDoneEvent& _event)
{
    m_CurrentWorkersScore[_event.GetSkill()] = _event.GetTotalValue();
}

//----------------------------------------------------------------------------
void GameDirector::OnMoneyBonusEvent(const MoneyBonusEvent& _event)
{
    m_CurrentAvailableMoney -= _event.GetBonus();
}

//----------------------------------------------------------------------------
void GameDirector::OnWorkerQuitEvent(const WorkerQuitEvent& _event)
{
    m_TotalSalaryToPay -= Global::WORKER_SALARY;    
}

//----------------------------------------------------------------------------
bool GameDirector::IsGameOver()
{
    return m_IsGameOver;
}

//----------------------------------------------------------------------------
void GameDirector::Reset()
{
    m_MetaCriticScore = "";
    m_UserScore = "";
    m_GameName = "";
    m_IsGameOver = false;
    m_TotalStressLevel = 0.0f;
    m_GameOverReason = "";
    m_CurrentAvailableMoney = Global::AVAILABLE_MONEY;
    m_FeatureList.clear();
    for (int i = 0; i < Global::NB_FEATURES; ++i)
    {
        Feature feature;
        feature.m_Type = static_cast<Skill>(rand() % Skills_Count);
        feature.m_Name = Global::FeatureTypeToString(feature.m_Type);
        feature.m_Score = static_cast<float>(Global::MIN_FEATURE_COST + rand() % static_cast<int>(Global::MAX_FEATURE_COST - Global::MIN_FEATURE_COST));

        if (const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>())
        {
            switch (feature.m_Type)
            {
            case Art: 
                feature.m_TypeIcon = catalog->Get(DataNameIds::Sprites::k_ArtIcon);
                break;
            case Design:
                feature.m_TypeIcon = catalog->Get(DataNameIds::Sprites::k_DesignIcon);
                break;
            case Code:
                feature.m_TypeIcon = catalog->Get(DataNameIds::Sprites::k_CodeIcon);
                break;
            }            
            feature.m_TypeIcon->As<Sprite>()->SetZValue(500);
        }

        m_FeatureList.push_back(feature);
    }

    for (int i = 0; i < Skill::Skills_Count; ++i)
    {
        m_CurrentWorkersScore[i] = 0;
    }

    m_TotalSalaryToPay = Global::NB_STARTING_WORKERS * Global::WORKER_SALARY;

    m_GameTimer.restart();

}


//----------------------------------------------------------------------------
void GameDirector::OnRestartGameEvent(const RestartGameEvent& _event)
{
    Reset();
}

//----------------------------------------------------------------------------
void GameDirector::OnWorkerTotalStressUpdateEvent(const WorkerTotalStressUpdateEvent& _event)
{
    m_TotalStressLevel = _event.GetTotalValue();
}

//----------------------------------------------------------------------------
void GameDirector::DebugLog()
{/*
    std::cout << "Feature List: " << std::endl;

    for (const Feature& feature : m_FeatureList)
    {
        std::cout << "  - " << feature.m_Name << " - Type:" << Global::FeatureTypeToString(feature.m_Type) << " - Score:" << static_cast<int>(feature.m_Score) << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Available Money: " << static_cast<int>(m_CurrentAvailableMoney) << std::endl;

    std::cout << std::endl;
    std::cout << std::endl;*/
}
