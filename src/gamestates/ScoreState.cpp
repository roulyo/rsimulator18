#include <gamestates/ScoreState.h>

#include <ForgeSample.h>
#include <forge/system/input/InputManager.h>


//----------------------------------------------------------------------------
ScoreState::ScoreState(const ForgeSample& _game)
    : AbstractGameState(_game)
{
    m_Systems.push_back(&m_RenderingSystem);
}

//----------------------------------------------------------------------------
ScoreState::~ScoreState()
{
}

//----------------------------------------------------------------------------
void ScoreState::BeginFrame(const long long& _dt)
{
    AbstractGameState::BeginFrame(_dt);
}

//----------------------------------------------------------------------------
void ScoreState::EndFrame(const long long& _dt)
{
    AbstractGameState::EndFrame(_dt);
}

//----------------------------------------------------------------------------
void ScoreState::OnPush()
{
    AbstractGameState::OnPush();

    InputManager::GetInstance().SetInputMapping(&m_ScoreInputScheme);
}

//----------------------------------------------------------------------------
void ScoreState::OnPop()
{
    AbstractGameState::OnPop();

    InputManager::GetInstance().SetInputMapping(nullptr);
}

