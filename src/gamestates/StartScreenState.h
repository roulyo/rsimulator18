#pragma once

#include <forge/core/GameState.h>

#include <gamestates/StartScreenInputs.h>

#include <systems/RenderingSystem.h>

//----------------------------------------------------------------------------
class ForgeSample;

//----------------------------------------------------------------------------
class StartScreenState : public AbstractGameState
{
public:
    StartScreenState(const ForgeSample& _game);
    ~StartScreenState();

    void BeginFrame(const long long& _dt) override;
    void EndFrame(const long long& _dt) override;

    void OnPush() override;
    void OnPop() override;
    
private:
    StartScreenInputScheme        m_StartScreenInputScheme;

    RenderingSystem         m_RenderingSystem;

};
