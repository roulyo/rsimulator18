#include <gamestates/MainGameState.h>

#include <ForgeSample.h>
#include <forge/system/input/InputManager.h>


//----------------------------------------------------------------------------
MainGameState::MainGameState(const ForgeSample& _game)
    : AbstractGameState(_game)
{
    m_Systems.push_back(&m_ClickableWorkerSystem); 
    m_Systems.push_back(&m_PlayerControlSystem);  
    m_Systems.push_back(&m_PhysicsSystem);  
    m_Systems.push_back(&m_RenderingSystem);
    m_Systems.push_back(&m_StressLevelSystem);
}

//----------------------------------------------------------------------------
MainGameState::~MainGameState()
{
}

//----------------------------------------------------------------------------
void MainGameState::BeginFrame(const long long& _dt)
{
    AbstractGameState::BeginFrame(_dt);
}

//----------------------------------------------------------------------------
void MainGameState::EndFrame(const long long& _dt)
{
    AbstractGameState::EndFrame(_dt);
}

//----------------------------------------------------------------------------
void MainGameState::OnPush()
{
    AbstractGameState::OnPush();

    InputManager::GetInstance().SetInputMapping(&m_MainGameInputScheme);
}

//----------------------------------------------------------------------------
void MainGameState::OnPop()
{
    AbstractGameState::OnPop();

    InputManager::GetInstance().SetInputMapping(nullptr);
}
