#include <gamestates/StartScreenInputs.h>

#include <forge/system/input/InputTypes.h>

//----------------------------------------------------------------------------
StartScreenInputScheme::StartScreenInputScheme()
{
    RegisterKeyInput(Keyboard::Key::Space, &m_StartKeyBinder);
}