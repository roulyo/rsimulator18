#include <gamestates/StartScreenState.h>

#include <ForgeSample.h>
#include <forge/system/input/InputManager.h>

//----------------------------------------------------------------------------
StartScreenState::StartScreenState(const ForgeSample& _game)
    : AbstractGameState(_game)
{
    m_Systems.push_back(&m_RenderingSystem);
}

//----------------------------------------------------------------------------
StartScreenState::~StartScreenState()
{

}

//----------------------------------------------------------------------------
void StartScreenState::BeginFrame(const long long& _dt)
{
    AbstractGameState::BeginFrame(_dt);
}

//----------------------------------------------------------------------------
void StartScreenState::EndFrame(const long long& _dt)
{
    AbstractGameState::EndFrame(_dt);
}

//----------------------------------------------------------------------------
void StartScreenState::OnPush()
{
    AbstractGameState::OnPush();

    InputManager::GetInstance().SetInputMapping(&m_StartScreenInputScheme);
}

//----------------------------------------------------------------------------
void StartScreenState::OnPop()
{
    AbstractGameState::OnPop();

    InputManager::GetInstance().SetInputMapping(nullptr);
}
