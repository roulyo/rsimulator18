#pragma once

#include <forge/system/input/InputMappingScheme.h>
#include <forge/system/input/KeyInputBinder.h>
#include <forge/system/input/MouseInputBinder.h>

#include <events/game/RestartGameEvent.h>

//----------------------------------------------------------------------------
 typedef KeyInputBinder<RestartGameEvent, RestartGameEvent> RestartKeyBinder;

//----------------------------------------------------------------------------
class ScoreInputScheme : public InputMappingScheme
{
public:
    ScoreInputScheme();

private:
    RestartKeyBinder m_RestartKeyBinder;

};