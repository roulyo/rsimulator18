#pragma once

#include <forge/core/GameState.h>

#include <gamestates/FreeRoamingInputs.h>

#include <systems/PlayerControlSystem.h>
#include <systems/RenderingSystem.h>
#include <systems/PhysicsSystem.h>
#include <systems/StressLevelSystem.h>


//----------------------------------------------------------------------------
class FreeRoamingState : public AbstractGameState
{
public:
    FreeRoamingState(const AbstractForgeGame& _game);
    ~FreeRoamingState();

    void BeginFrame(const long long& _dt) override;
    void EndFrame(const long long& _dt) override;

    void OnPush() override;
    void OnPop() override;

private:
    FreeRoamingInputScheme  m_FreeRoamingInputScheme;

    PlayerControlSystem     m_PlayerControlSystem;
    PhysicsSystem           m_PhysicsSystem;
    RenderingSystem         m_RenderingSystem;
    StressLevelSystem       m_StressLevelSystem;
};
