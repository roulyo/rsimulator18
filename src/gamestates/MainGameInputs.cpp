#include <gamestates/MainGameInputs.h>

#include <forge/system/input/InputTypes.h>

//----------------------------------------------------------------------------
MainGameInputScheme::MainGameInputScheme()
{
    RegisterMouseInput(Mouse::Left, &m_LeftClickBlinder);
    RegisterMouseInput(Mouse::Right, &m_RightClickBlinder);

    RegisterKeyInput(Keyboard::Q, &m_MoveWestBinder);
    RegisterKeyInput(Keyboard::S, &m_MoveSouthBinder);
    RegisterKeyInput(Keyboard::D, &m_MoveEastBinder);
    RegisterKeyInput(Keyboard::Z, &m_MoveNorthBinder);
}
