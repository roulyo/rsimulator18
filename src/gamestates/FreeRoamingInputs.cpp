#include <gamestates/FreeRoamingInputs.h>

#include <forge/system/input/InputTypes.h>

//----------------------------------------------------------------------------
FreeRoamingInputScheme::FreeRoamingInputScheme()
{
    RegisterKeyInput(Keyboard::Q, &m_MoveWestBinder);
    RegisterKeyInput(Keyboard::S, &m_MoveSouthBinder);
    RegisterKeyInput(Keyboard::D, &m_MoveEastBinder);
    RegisterKeyInput(Keyboard::Z, &m_MoveNorthBinder);
}
