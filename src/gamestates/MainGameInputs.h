#pragma once

#include <forge/system/input/InputMappingScheme.h>
#include <forge/system/input/KeyInputBinder.h>
#include <forge/system/input/MouseInputBinder.h>

#include <events/inputs/MoveEvent.h>
#include <events/inputs/MouseEvent.h>

//----------------------------------------------------------------------------
 typedef MouseInputBinder<ClickedMouseEvent<true, MouseButton::Left >, ClickedMouseEvent<false, MouseButton::Left >> LeftClickBinder;
 typedef MouseInputBinder<ClickedMouseEvent<true, MouseButton::Right >, ClickedMouseEvent<false, MouseButton::Right >> RightClickBinder;

 typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::North>, OrientedMoveEvent<false, MoveDirection::North>> MoveNorthBinder;
 typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::East>, OrientedMoveEvent<false, MoveDirection::East>>  MoveEastBinder;
 typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::South>, OrientedMoveEvent<false, MoveDirection::South>> MoveSouthBinder;
 typedef KeyInputBinder<OrientedMoveEvent<true, MoveDirection::West>, OrientedMoveEvent<false, MoveDirection::West>>  MoveWestBinder;

//----------------------------------------------------------------------------
class MainGameInputScheme : public InputMappingScheme
{
public:
    MainGameInputScheme();

private:
    LeftClickBinder m_LeftClickBlinder;
    RightClickBinder m_RightClickBlinder;

    MoveNorthBinder m_MoveNorthBinder;
    MoveEastBinder  m_MoveEastBinder;
    MoveSouthBinder m_MoveSouthBinder;
    MoveWestBinder  m_MoveWestBinder;

};