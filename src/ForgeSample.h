#include <forge/core/Game.h>

#include <gamestates/StartScreenState.h>
#include <gamestates/MainGameState.h>
#include <gamestates/ScoreState.h>

//----------------------------------------------------------------------------
class GameDirector;
class RestartGameEvent;

//----------------------------------------------------------------------------
class ForgeSample : public AbstractForgeGame
{
public:
    ForgeSample(); 
    ~ForgeSample();
    
    void Init(const std::string& _gameName) override;
    void Update(const long long& _dt) override;
    void Quit() override;
    
    void OnRestartGameEvent(const RestartGameEvent& _event);

private:
    void InitStartWorld();
    void InitGameWorld();
    void ResetGameWorld();

    void AddBackgroundAssets();
    void AddSplashScreenEntity();
    void AddWorkerGroup(float _x, float _y);
    void ChangeLight(unsigned short _intensity);

private:
    StartScreenState m_StartScreenState;
    MainGameState    m_MainGameState;
    ScoreState       m_ScoreState;
    GameDirector*    m_GameDirector;

};
