#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/2d/drawable/Texture.h>

//----------------------------------------------------------------------------
class DefaultSpriteSheetFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_SpriteSheet;

};

//----------------------------------------------------------------------------
class TableTextureFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_TableTexture;
};

//----------------------------------------------------------------------------
class FloorTextureFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_Floor;

};

//----------------------------------------------------------------------------
class IconTextureFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_Texture;

};

//----------------------------------------------------------------------------
class SplashScreenTextureFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_SplashScreenTexture;

};

//----------------------------------------------------------------------------
class ScoreScreenTextureFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_ScoreScreenTexture;

};

//----------------------------------------------------------------------------
class ScoreBackgroundTextureFactory : public AbstractDataFactory
{
public:
    Texture::Ptr Create() const override;

private:
    static Texture::Ptr m_Texture;

};
