#include <data/textures/TextureFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Texture::Ptr DefaultSpriteSheetFactory::m_SpriteSheet = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr DefaultSpriteSheetFactory::Create() const
{
    if (m_SpriteSheet == nullptr)
    {
        Texture* texture = new Texture("assets/spritesheet.png");
        texture->SetDataNameId(Textures::k_DefaultSpriteSheet);

        m_SpriteSheet.reset(texture);
    }

    return m_SpriteSheet;
}

//----------------------------------------------------------------------------
Texture::Ptr FloorTextureFactory::m_Floor = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr FloorTextureFactory::Create() const
{
    if (m_Floor == nullptr)
    {
        Texture* texture = new Texture("assets/background.png");
        texture->SetDataNameId(Textures::k_FloorSpriteSheet);

        m_Floor.reset(texture);
    }

    return m_Floor;
}

//----------------------------------------------------------------------------
Texture::Ptr TableTextureFactory::m_TableTexture = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr TableTextureFactory::Create() const
{
    if (m_TableTexture == nullptr)
    {
        Texture* texture = new Texture("assets/little_table.png");
        texture->SetDataNameId(Textures::k_DefaultSpriteSheet);

        m_TableTexture.reset(texture);
    }

    return m_TableTexture;
}

//----------------------------------------------------------------------------
Texture::Ptr IconTextureFactory::m_Texture = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr IconTextureFactory::Create() const
{
    if (m_Texture == nullptr)
    {
        Texture* texture = new Texture("assets/icons.png");
        texture->SetDataNameId(Textures::k_Icons);

        m_Texture.reset(texture);
    }

    return m_Texture;
}

//----------------------------------------------------------------------------
Texture::Ptr SplashScreenTextureFactory::m_SplashScreenTexture = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr SplashScreenTextureFactory::Create() const
{
    if (m_SplashScreenTexture == nullptr)
    {
        Texture* texture = new Texture("assets/splashscreen.png");
        texture->SetDataNameId(Textures::k_SplashScreen);

        m_SplashScreenTexture.reset(texture);
    }

    return m_SplashScreenTexture;
}

//----------------------------------------------------------------------------
Texture::Ptr ScoreScreenTextureFactory::m_ScoreScreenTexture = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr ScoreScreenTextureFactory::Create() const
{
    if (m_ScoreScreenTexture == nullptr)
    {
        Texture* texture = new Texture("assets/scorescreen.png");
        texture->SetDataNameId(Textures::k_ScoreScreen);

        m_ScoreScreenTexture.reset(texture);
    }

    return m_ScoreScreenTexture;
}

//----------------------------------------------------------------------------
Texture::Ptr ScoreBackgroundTextureFactory::m_Texture = nullptr;

//----------------------------------------------------------------------------
Texture::Ptr ScoreBackgroundTextureFactory::Create() const
{
    if (m_Texture == nullptr)
    {
        Texture* texture = new Texture("assets/score_bg.png");
        texture->SetDataNameId(Textures::k_ScoreBackground);

        m_Texture.reset(texture);
    }

    return m_Texture;
}
