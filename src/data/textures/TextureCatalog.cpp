#include <data/textures/TextureCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
TextureCatalog::TextureCatalog()
{
    RegisterData(Textures::k_DefaultSpriteSheet, m_DefaultSpriteSheetFactory);
    RegisterData(Textures::k_FloorSpriteSheet, m_FloorSpriteSheetFactory);
    RegisterData(Textures::k_Table, m_TableTextureFactory);
    RegisterData(Textures::k_Icons, m_IconTextureFactory);
    RegisterData(Textures::k_SplashScreen, m_SplashScreenTextureFactory);
    RegisterData(Textures::k_ScoreScreen, m_ScoreScreenTextureFactory);
    RegisterData(Textures::k_ScoreBackground, m_ScoreBackgroundTextureFactory);
}

//----------------------------------------------------------------------------
TextureCatalog::~TextureCatalog()
{
    m_Factories.clear();
}
