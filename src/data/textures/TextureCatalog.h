#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/2d/drawable/Texture.h>

#include <data/textures/TextureFactories.h>

//----------------------------------------------------------------------------
class TextureCatalog : public DataCatalog<Texture>
{
    forge_DeclCatalog(TextureCatalog);

public:
    TextureCatalog();
    ~TextureCatalog();

private:
    DefaultSpriteSheetFactory       m_DefaultSpriteSheetFactory;
    FloorTextureFactory             m_FloorSpriteSheetFactory;
    TableTextureFactory             m_TableTextureFactory;
    IconTextureFactory              m_IconTextureFactory;
    SplashScreenTextureFactory      m_SplashScreenTextureFactory;
    ScoreScreenTextureFactory       m_ScoreScreenTextureFactory;
    ScoreBackgroundTextureFactory   m_ScoreBackgroundTextureFactory;
};
