#pragma once

#include <forge/system/data/Data.h>

namespace DataNameIds
{
    // Textures
    namespace Textures
    {
        extern const DataNameId k_DefaultSpriteSheet;
        extern const DataNameId k_FloorSpriteSheet;
        extern const DataNameId k_Table;
        extern const DataNameId k_Icons;
        extern const DataNameId k_SplashScreen;
        extern const DataNameId k_ScoreScreen;
        extern const DataNameId k_ScoreBackground;
    }

    // Animations
    namespace Animations
    {
        extern const DataNameId k_IdleFront;
        extern const DataNameId k_IdleBack;
        extern const DataNameId k_FXAnim;
    }

    // Sprites
    namespace Sprites
    {
        extern const DataNameId k_Whip;
        extern const DataNameId k_Coin;
        extern const DataNameId k_Character;
        extern const DataNameId k_Floor;
        extern const DataNameId k_CharacterGreen;
        extern const DataNameId k_CharacterRed;
        extern const DataNameId k_CharacterBlue;
        extern const DataNameId k_Table;
        extern const DataNameId k_ArtIcon;
        extern const DataNameId k_CodeIcon;
        extern const DataNameId k_DesignIcon;
        extern const DataNameId k_SplashScreen;
        extern const DataNameId k_ScoreScreen;
        extern const DataNameId k_ScoreBackground;
    }

    // Entities
    namespace Entities
    {
        extern const DataNameId k_Character;
        extern const DataNameId k_Worker;
        extern const DataNameId k_Floor;
        extern const DataNameId k_Table;
        extern const DataNameId k_SplashScreen;
    }

    // SoundBuffers
    namespace SoundBuffers
    {
        extern const DataNameId k_Whip;
        extern const DataNameId k_Coin;
        extern const DataNameId k_Feature;
        extern const DataNameId k_Death;
        extern const DataNameId k_Keyboard;
    }

    // Sounds
    namespace Sounds
    {
        extern const DataNameId k_Whip;
        extern const DataNameId k_Coin;
        extern const DataNameId k_Feature;
        extern const DataNameId k_Death;
        extern const DataNameId k_Keyboard;
    }

    // Musics
    namespace Musics
    {
        extern const DataNameId k_FunkyLoop;
    }

    // Fonts
    namespace Fonts
    {
        extern const DataNameId k_Pixelli;
        extern const DataNameId k_Number;
        extern const DataNameId k_SansSerif;
    }

    // FXs
    namespace FXs
    {
        extern const DataNameId k_Whip;
        extern const DataNameId k_Coin;
    }
}
