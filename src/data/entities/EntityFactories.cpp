#include <data/entities/EntityFactories.h>

#include <data/sprites/SpriteCatalog.h>

#include <components/PlayableCharacterComponent.h>
#include <components/PhysicableComponent.h>
#include <components/RenderableComponent.h>
#include <components/WorkerComponent.h>
#include <components/ClickableComponent.h>

#include <forge/system/data/DataManager.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Entity::Ptr CharacterEntityFactory::Create() const
{
    const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();

    Entity* character = new Entity();
    character->SetDataNameId(Entities::k_Character);

    RenderableComponent& renderComp = character->AddComponent<RenderableComponent>();
    if (catalog != nullptr)
    {
        renderComp.SetSprite(catalog->Get(Sprites::k_CharacterGreen));
    }

    character->AddComponent<PhysicableComponent>();
    character->AddComponent<PlayableCharacterComponent>();
    character->AddComponent<ClickableComponent>();
    character->AddComponent<WorkerComponent>();

    character->SetScale(64.0f, 64.0f);

    return Entity::Ptr(character);
}

//----------------------------------------------------------------------------
const DataNameId WorkerEntityFactory::ids[3] =
        {
            DataNameIds::Sprites::k_CharacterGreen,
            DataNameIds::Sprites::k_CharacterRed,
            DataNameIds::Sprites::k_CharacterBlue
        };

//----------------------------------------------------------------------------
Entity::Ptr WorkerEntityFactory::Create() const
{
    const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();

    Entity* worker = new Entity();
    worker->SetDataNameId(Entities::k_Worker);

    RenderableComponent& renderComp = worker->AddComponent<RenderableComponent>();
    if (catalog != nullptr)
    {
        renderComp.SetSprite(catalog->Get(ids[rand() % 3]));
    }

    worker->AddComponent<ClickableComponent>();
    worker->AddComponent<WorkerComponent>();

    worker->SetScale(64.0f, 64.0f);
    return Entity::Ptr(worker);
}

//----------------------------------------------------------------------------
Entity::Ptr FloorEntityFactory::Create() const
{
    const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();

    Entity* floor = new Entity();
    floor->SetDataNameId(Entities::k_Floor);

    RenderableComponent& renderComp = floor->AddComponent<RenderableComponent>();
    if (catalog != nullptr)
    {
        renderComp.SetSprite(catalog->Get(Sprites::k_Floor));
    }
     
    return Entity::Ptr(floor);
}

//----------------------------------------------------------------------------
Entity::Ptr TableEntityFactory::Create() const
{
    const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();

    Entity* entity = new Entity();
    entity->SetDataNameId(Entities::k_Table);

    RenderableComponent& renderComp = entity->AddComponent<RenderableComponent>();
    if (catalog != nullptr)
    {
        renderComp.SetSprite(catalog->Get(DataNameIds::Sprites::k_Table));
    }

    entity->SetScale(550.0f, 330.0f);
    return Entity::Ptr(entity);
}

//----------------------------------------------------------------------------
Entity::Ptr SplashScreenEntityFactory::Create() const
{
    const SpriteCatalog* catalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();

    Entity* entity = new Entity();
    entity->SetDataNameId(Entities::k_SplashScreen);

    RenderableComponent& renderComp = entity->AddComponent<RenderableComponent>();
    if (catalog != nullptr)
    {
        renderComp.SetSprite(catalog->Get(DataNameIds::Sprites::k_SplashScreen));
    }

    entity->SetScale(1280.0f, 720.0f);
    return Entity::Ptr(entity);
}
