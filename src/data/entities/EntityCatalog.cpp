#include <data/entities/EntityCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
EntityCatalog::EntityCatalog()
{
    RegisterData(Entities::k_Character, m_CharacterEntityFactory);
    RegisterData(Entities::k_Worker, m_WorkerEntityFactory);
    RegisterData(Entities::k_Floor, m_FloorEntityFactory);
    RegisterData(Entities::k_Table, m_TableEntityFactory);
    RegisterData(Entities::k_SplashScreen, m_SplashScreenEntityFactory);
}

//----------------------------------------------------------------------------
EntityCatalog::~EntityCatalog()
{
    m_Factories.clear();
}
