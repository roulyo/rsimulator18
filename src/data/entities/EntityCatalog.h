#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/core/ecs/Entity.h>

#include <data/entities/EntityFactories.h>

//----------------------------------------------------------------------------
class EntityCatalog : public DataCatalog<Entity>
{
    forge_DeclCatalog(EntityCatalog);

public:
    EntityCatalog();
    ~EntityCatalog();

private:
    CharacterEntityFactory  m_CharacterEntityFactory;
    WorkerEntityFactory     m_WorkerEntityFactory;
    FloorEntityFactory      m_FloorEntityFactory;
    TableEntityFactory      m_TableEntityFactory;
    SplashScreenEntityFactory      m_SplashScreenEntityFactory;
};
