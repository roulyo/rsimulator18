#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/core/ecs/Entity.h>

#include <data/DataNameIds.h>


//----------------------------------------------------------------------------
class CharacterEntityFactory : public AbstractDataFactory
{
public:
    Entity::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class WorkerEntityFactory : public AbstractDataFactory
{
public:
    Entity::Ptr Create() const override;
private:
    static const DataNameId ids[3];
};

//----------------------------------------------------------------------------
class FloorEntityFactory : public AbstractDataFactory
{
public:
    Entity::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class TableEntityFactory : public AbstractDataFactory
{
public:
    Entity::Ptr Create() const override;
    
};

//----------------------------------------------------------------------------
class SplashScreenEntityFactory : public AbstractDataFactory
{
public:
    Entity::Ptr Create() const override;

};

