#include <data/fonts/FontCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
FontCatalog::FontCatalog()
{
    RegisterData(Fonts::k_Pixelli, m_PixelliFontFactory);
    RegisterData(Fonts::k_SansSerif, m_SansSerifFactory);
}

//----------------------------------------------------------------------------
FontCatalog::~FontCatalog()
{
    m_Factories.clear();
}
