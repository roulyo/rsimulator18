#include <data/fonts/FontFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Font::Ptr PixelliFontFactory::m_Font = nullptr;

//----------------------------------------------------------------------------
Font::Ptr PixelliFontFactory::Create() const
{
    if (m_Font == nullptr)
    {
        Font* font = new Font("assets/pixel_maz.ttf");
        font->SetDataNameId(Fonts::k_Pixelli);

        m_Font.reset(font);
    }

    return m_Font;
}

//----------------------------------------------------------------------------
Font::Ptr NumberFontFactory::m_Font = nullptr;

//----------------------------------------------------------------------------
Font::Ptr NumberFontFactory::Create() const
{
    if (m_Font == nullptr)
    {
        Font* font = new Font("assets/monospace_pixel.woff");
        font->SetDataNameId(Fonts::k_Number);

        m_Font.reset(font);
    }

    return m_Font;
}

//----------------------------------------------------------------------------
Font::Ptr SansSerifFactory::m_Font = nullptr;

//----------------------------------------------------------------------------
Font::Ptr SansSerifFactory::Create() const
{
    if (m_Font == nullptr)
    {
        Font* font = new Font("assets/sans_serif.ttf");
        font->SetDataNameId(Fonts::k_SansSerif);

        m_Font.reset(font);
    }

    return m_Font;
}