#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/2d/drawable/Font.h>

#include <data/fonts/FontFactories.h>

//----------------------------------------------------------------------------
class FontCatalog : public DataCatalog<Font>
{
    forge_DeclCatalog(FontCatalog);

public:
    FontCatalog();
    ~FontCatalog();

private:
    PixelliFontFactory  m_PixelliFontFactory;
    SansSerifFactory    m_SansSerifFactory;
    
};
