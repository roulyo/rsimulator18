#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/2d/drawable/Animation.h>

//----------------------------------------------------------------------------
class IdleFrontAnimationFactory : public AbstractDataFactory
{
public:
    Animation::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class IdleBackAnimationFactory : public AbstractDataFactory
{
public:
    Animation::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class FXAnimationFactory : public AbstractDataFactory
{
public:
    Animation::Ptr Create() const override;
};
