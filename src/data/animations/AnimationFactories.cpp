#include <data/animations/AnimationFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Animation::Ptr IdleFrontAnimationFactory::Create() const
{
    Animation* idle = new Animation(0, 0, 2, 500);
    idle->SetDataNameId(Animations::k_IdleFront);

    return Animation::Ptr(idle);
}

//----------------------------------------------------------------------------
Animation::Ptr IdleBackAnimationFactory::Create() const
{
    Animation* walkNorth = new Animation(0, 1, 2, 500);
    walkNorth->SetDataNameId(Animations::k_IdleBack);

    return Animation::Ptr(walkNorth);
}

//----------------------------------------------------------------------------
Animation::Ptr FXAnimationFactory::Create() const
{
    Animation* animation = new Animation(0, 0, 6, 500);
    animation->SetDataNameId(Animations::k_FXAnim);

    animation->SetIsRepeatable(false);

    return Animation::Ptr(animation);
}
