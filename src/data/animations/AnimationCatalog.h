#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/2d/drawable/Animation.h>

#include <data/animations/AnimationFactories.h>

//----------------------------------------------------------------------------
class AnimationCatalog : public DataCatalog<Animation>
{
    forge_DeclCatalog(AnimationCatalog);

public:
    AnimationCatalog();
    ~AnimationCatalog();

private:
    IdleFrontAnimationFactory   m_IdleAnimationFactory;
    IdleBackAnimationFactory    m_WalkNorthAnimationFactory;
    FXAnimationFactory          m_FXAnimationFactory;
};
