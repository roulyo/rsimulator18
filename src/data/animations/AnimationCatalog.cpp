#include <data/animations/AnimationCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
AnimationCatalog::AnimationCatalog()
{
    RegisterData(Animations::k_IdleFront,   m_IdleAnimationFactory);
    RegisterData(Animations::k_IdleBack,    m_WalkNorthAnimationFactory);
    RegisterData(Animations::k_FXAnim,      m_FXAnimationFactory);
}

//----------------------------------------------------------------------------
AnimationCatalog::~AnimationCatalog()
{
    m_Factories.clear();
}
