#include <data/sprites/SpriteCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
SpriteCatalog::SpriteCatalog()
{
    RegisterData(Sprites::k_Coin, m_CoinSpriteFactory);
    RegisterData(Sprites::k_Whip, m_WhipSpriteFactory);
    RegisterData(Sprites::k_Floor, m_FloorSpriteFactory);
    RegisterData(Sprites::k_CharacterGreen, m_CharacterGreenSpriteFactory);
    RegisterData(Sprites::k_CharacterRed, m_CharacterRedSpriteFactory);
    RegisterData(Sprites::k_CharacterBlue, m_CharacterBlueSpriteFactory);
    RegisterData(Sprites::k_Table, m_TableSpriteFactory);
    RegisterData(Sprites::k_ArtIcon, m_ArtIconSpriteFactory);
    RegisterData(Sprites::k_CodeIcon, m_CodeIconSpriteFactory);
    RegisterData(Sprites::k_DesignIcon, m_DesignIconSpriteFactory);
    RegisterData(Sprites::k_SplashScreen, m_SplashScreenSpriteFactory);
    RegisterData(Sprites::k_ScoreScreen, m_ScoreScreenSpriteFactory);
    RegisterData(Sprites::k_ScoreBackground, m_ScoreBackgroundSpriteFactory);
}

//----------------------------------------------------------------------------
SpriteCatalog::~SpriteCatalog()
{
    m_Factories.clear();
}
