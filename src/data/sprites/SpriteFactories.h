#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/2d/drawable/Sprite.h>

//----------------------------------------------------------------------------
class CharacterGreenSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class CharacterRedSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class CharacterBlueSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class TableSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class SplashScreenSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class ScoreScreenSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class FloorSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class CoinSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class WhipSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class ArtIconSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class CodeIconSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class DesignIconSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class ScoreBackgroundSpriteFactory : public AbstractDataFactory
{
public:
    Sprite::Ptr Create() const override;
};
