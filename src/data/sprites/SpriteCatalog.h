#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/2d/drawable/Sprite.h>

#include <data/sprites/SpriteFactories.h>

//----------------------------------------------------------------------------
class SpriteCatalog : public DataCatalog<Sprite>
{
    forge_DeclCatalog(SpriteCatalog);

public:
    SpriteCatalog();
    ~SpriteCatalog();

private:
    CoinSpriteFactory               m_CoinSpriteFactory;
    WhipSpriteFactory               m_WhipSpriteFactory;
    FloorSpriteFactory              m_FloorSpriteFactory;
    CharacterGreenSpriteFactory     m_CharacterGreenSpriteFactory;
    CharacterRedSpriteFactory       m_CharacterRedSpriteFactory;
    CharacterBlueSpriteFactory      m_CharacterBlueSpriteFactory;
    TableSpriteFactory              m_TableSpriteFactory;
    ArtIconSpriteFactory            m_ArtIconSpriteFactory;
    CodeIconSpriteFactory           m_CodeIconSpriteFactory;
    DesignIconSpriteFactory         m_DesignIconSpriteFactory;
    SplashScreenSpriteFactory       m_SplashScreenSpriteFactory;
    ScoreScreenSpriteFactory        m_ScoreScreenSpriteFactory;
    ScoreBackgroundSpriteFactory    m_ScoreBackgroundSpriteFactory;

};
