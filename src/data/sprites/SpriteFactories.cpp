#include <data/sprites/SpriteFactories.h>

#include <data/textures/TextureCatalog.h>
#include <data/animations/AnimationCatalog.h>
#include <data/DataNameIds.h>

#include <forge/system/data/DataManager.h>

#include <string>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Sprite::Ptr CharacterGreenSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* character;

    if (texCatalog != nullptr)
    {
        character = new Sprite(texCatalog->Get(Textures::k_DefaultSpriteSheet), sf::IntRect(64 * 0, 64 * 0, 64, 64));
        character->SetDataNameId(Sprites::k_CharacterGreen);
        character->SetOverlayColor(255, 255, 255);
        character->SetScale(0.5f, 0.5f);

        const AnimationCatalog* spriteCatalog = DataManager::GetInstance().GetCatalog<AnimationCatalog>();

        if (spriteCatalog != nullptr)
        {
            character->AddAnimation(spriteCatalog->Get(Animations::k_IdleFront));
            character->AddAnimation(spriteCatalog->Get(Animations::k_IdleBack));
        }
    }

    return Sprite::Ptr(character);
}

//----------------------------------------------------------------------------
Sprite::Ptr CharacterRedSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* character;

    if (texCatalog != nullptr)
    {
        character = new Sprite(texCatalog->Get(Textures::k_DefaultSpriteSheet), sf::IntRect(64 * 2, 64 * 0, 64, 64));
        character->SetDataNameId(Sprites::k_CharacterRed);
        character->SetOverlayColor(255, 255, 255);
        character->SetScale(0.5f, 0.5f);

        const AnimationCatalog* spriteCatalog = DataManager::GetInstance().GetCatalog<AnimationCatalog>();

        if (spriteCatalog != nullptr)
        {
            character->AddAnimation(spriteCatalog->Get(Animations::k_IdleFront));
            character->AddAnimation(spriteCatalog->Get(Animations::k_IdleBack));
        }
    }

    return Sprite::Ptr(character);
}

//----------------------------------------------------------------------------
Sprite::Ptr CharacterBlueSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* character;

    if (texCatalog != nullptr)
    {
        character = new Sprite(texCatalog->Get(Textures::k_DefaultSpriteSheet), sf::IntRect(64 * 4, 64 * 0, 64, 64));
        character->SetDataNameId(Sprites::k_CharacterBlue);
        character->SetOverlayColor(255, 255, 255);
        character->SetScale(0.5f, 0.5f);

        const AnimationCatalog* spriteCatalog = DataManager::GetInstance().GetCatalog<AnimationCatalog>();

        if (spriteCatalog != nullptr)
        {
            character->AddAnimation(spriteCatalog->Get(Animations::k_IdleFront));
            character->AddAnimation(spriteCatalog->Get(Animations::k_IdleBack));
        }
    }

    return Sprite::Ptr(character);
}

//----------------------------------------------------------------------------
Sprite::Ptr FloorSpriteFactory::Create() const
{
    const TextureCatalog* catalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* floor = nullptr;

    if (catalog != nullptr)
    {
        floor = new Sprite(catalog->Get(Textures::k_FloorSpriteSheet), sf::IntRect(0, 0, 2000, 2000));
        floor->SetDataNameId(Sprites::k_Floor);
    }

    return Sprite::Ptr(floor);
}


//----------------------------------------------------------------------------
Sprite::Ptr TableSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* sprite;

    if (texCatalog != nullptr)
    {
        sprite = new Sprite(texCatalog->Get(Textures::k_Table), sf::IntRect(0, 0, 550, 330));
        sprite->SetDataNameId(Sprites::k_Table);
        sprite->SetOverlayColor(255, 255, 255);
    }

    return Sprite::Ptr(sprite);
}

//----------------------------------------------------------------------------
Sprite::Ptr WhipSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* sprite;

    if (texCatalog != nullptr)
    {
        sprite = new Sprite(texCatalog->Get(Textures::k_DefaultSpriteSheet), sf::IntRect(0 * 64, 3 * 64, 64, 64));
        sprite->SetDataNameId(Sprites::k_Whip);

        const AnimationCatalog* spriteCatalog = DataManager::GetInstance().GetCatalog<AnimationCatalog>();

        if (spriteCatalog != nullptr)
        {
            sprite->AddAnimation(spriteCatalog->Get(Animations::k_FXAnim));
        }
    }

    return Sprite::Ptr(sprite);
}

//----------------------------------------------------------------------------
Sprite::Ptr CoinSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* sprite;

    if (texCatalog != nullptr)
    {
        sprite = new Sprite(texCatalog->Get(Textures::k_DefaultSpriteSheet), sf::IntRect(0 * 64, 2 * 64, 64, 64));
        sprite->SetDataNameId(Sprites::k_Coin);

        const AnimationCatalog* spriteCatalog = DataManager::GetInstance().GetCatalog<AnimationCatalog>();

        if (spriteCatalog != nullptr)
        {
            sprite->AddAnimation(spriteCatalog->Get(Animations::k_FXAnim));
        }
    }

    return Sprite::Ptr(sprite);
}

//----------------------------------------------------------------------------
Sprite::Ptr SplashScreenSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* sprite;

    if (texCatalog != nullptr)
    {
        sprite = new Sprite(texCatalog->Get(Textures::k_SplashScreen), sf::IntRect(0, 0, 1280, 720));
        sprite->SetDataNameId(Sprites::k_SplashScreen);
    }

    return Sprite::Ptr(sprite);
}

//----------------------------------------------------------------------------
Sprite::Ptr ArtIconSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* sprite;

    if (texCatalog != nullptr)
    {
        sprite = new Sprite(texCatalog->Get(Textures::k_Icons), sf::IntRect(0, 0, 16, 16));
        sprite->SetDataNameId(Sprites::k_ArtIcon);
    }

    return Sprite::Ptr(sprite);
}

//----------------------------------------------------------------------------
Sprite::Ptr CodeIconSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* sprite;

    if (texCatalog != nullptr)
    {
        sprite = new Sprite(texCatalog->Get(Textures::k_Icons), sf::IntRect(16, 0, 16, 16));
        sprite->SetDataNameId(Sprites::k_CodeIcon);
    }

    return Sprite::Ptr(sprite);
}


//----------------------------------------------------------------------------
Sprite::Ptr DesignIconSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* sprite;

    if (texCatalog != nullptr)
    {
        sprite = new Sprite(texCatalog->Get(Textures::k_Icons), sf::IntRect(32, 0, 16, 16));
        sprite->SetDataNameId(Sprites::k_DesignIcon);
    }

    return Sprite::Ptr(sprite);
}

//----------------------------------------------------------------------------
Sprite::Ptr ScoreScreenSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* sprite;

    if (texCatalog != nullptr)
    {
        sprite = new Sprite(texCatalog->Get(Textures::k_ScoreScreen), sf::IntRect(0, 0, 854, 720));
        sprite->SetDataNameId(Sprites::k_ScoreScreen);
    }

    return Sprite::Ptr(sprite);
}

//----------------------------------------------------------------------------
Sprite::Ptr ScoreBackgroundSpriteFactory::Create() const
{
    const TextureCatalog* texCatalog = DataManager::GetInstance().GetCatalog<TextureCatalog>();

    Sprite* sprite;

    if (texCatalog != nullptr)
    {
        sprite = new Sprite(texCatalog->Get(Textures::k_ScoreBackground), sf::IntRect(0, 0, 84, 84));
        sprite->SetDataNameId(Sprites::k_ScoreBackground);
    }

    return Sprite::Ptr(sprite);
}
