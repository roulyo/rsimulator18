#include <data/fxs/FXFactories.h>

#include <data/sprites/SpriteCatalog.h>
#include <data/audio/SoundCatalog.h>
#include <data/DataNameIds.h>

#include <forge/system/data/DataManager.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
FX::Ptr WhipFXFactory::Create() const
{
    const SpriteCatalog* spriteCatalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();
    const SoundCatalog*  soundCatalog = DataManager::GetInstance().GetCatalog<SoundCatalog>();

    FX* fx;

    if (spriteCatalog != nullptr && soundCatalog != nullptr)
    {
        Sprite::Ptr sprite  = spriteCatalog->Get(DataNameIds::Sprites::k_Whip);
        Sound::Ptr  sound   = soundCatalog->Get(DataNameIds::Sounds::k_Whip);

        fx = new FX(sprite, sound, 500); //sound->As<Sound>()->Get().getDuration().asMilliseconds());
        fx->SetDataNameId(FXs::k_Whip);

        sprite->As<Sprite>()->PlayAnimation(Animations::k_FXAnim);
        sprite->As<Sprite>()->SetZValue(100);
    }

    return FX::Ptr(fx);
}

//----------------------------------------------------------------------------
FX::Ptr CoinFXFactory::Create() const
{
    const SpriteCatalog* spriteCatalog = DataManager::GetInstance().GetCatalog<SpriteCatalog>();
    const SoundCatalog*  soundCatalog = DataManager::GetInstance().GetCatalog<SoundCatalog>();

    FX* fx;

    if (spriteCatalog != nullptr && soundCatalog != nullptr)
    {
        Sprite::Ptr sprite  = spriteCatalog->Get(DataNameIds::Sprites::k_Coin);
        Sound::Ptr  sound   = soundCatalog->Get(DataNameIds::Sounds::k_Coin);

        fx = new FX(sprite, sound, 500); //sound->As<Sound>()->Get().getDuration().asMilliseconds());
        fx->SetDataNameId(FXs::k_Coin);

        sprite->As<Sprite>()->PlayAnimation(Animations::k_FXAnim);
        sprite->As<Sprite>()->SetZValue(100);
    }

    return FX::Ptr(fx);
}
