#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/core/fx/FX.h>

//----------------------------------------------------------------------------
class WhipFXFactory : public AbstractDataFactory
{
public:
    FX::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class CoinFXFactory : public AbstractDataFactory
{
public:
    FX::Ptr Create() const override;
};
