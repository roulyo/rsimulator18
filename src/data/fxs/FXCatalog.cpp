#include <data/fxs/FXCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
FXCatalog::FXCatalog()
{
    RegisterData(FXs::k_Coin, m_CoinFXFactory);
    RegisterData(FXs::k_Whip, m_WhipFXFactory);
}

//----------------------------------------------------------------------------
FXCatalog::~FXCatalog()
{
    m_Factories.clear();
}
