#include <data/audio/MusicFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Music::Ptr FunkyLoopMusicFactory::m_Music = nullptr;

//----------------------------------------------------------------------------
Music::Ptr FunkyLoopMusicFactory::Create() const
{
    if (m_Music == nullptr)
    {
        Music* music = new Music("assets/funkyloop.wav");
        music->SetDataNameId(Musics::k_FunkyLoop);

        m_Music.reset(music);
    }

    return m_Music;
}
