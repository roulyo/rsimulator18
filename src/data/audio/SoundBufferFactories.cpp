#include <data/audio/SoundBufferFactories.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
SoundBuffer::Ptr WhipSoundBufferFactory::m_SoundBuffer = nullptr;

//----------------------------------------------------------------------------
SoundBuffer::Ptr WhipSoundBufferFactory::Create() const
{
    if (m_SoundBuffer == nullptr)
    {
        SoundBuffer* soundBuffer = new SoundBuffer("assets/whip2.wav");
        soundBuffer->SetDataNameId(SoundBuffers::k_Whip);

        m_SoundBuffer.reset(soundBuffer);
    }

    return m_SoundBuffer;
}

//----------------------------------------------------------------------------
SoundBuffer::Ptr CoinSoundBufferFactory::m_SoundBuffer = nullptr;

//----------------------------------------------------------------------------
SoundBuffer::Ptr CoinSoundBufferFactory::Create() const
{
    if (m_SoundBuffer == nullptr)
    {
        SoundBuffer* soundBuffer = new SoundBuffer("assets/coin.wav");
        soundBuffer->SetDataNameId(SoundBuffers::k_Coin);

        m_SoundBuffer.reset(soundBuffer);
    }

    return m_SoundBuffer;
}

//----------------------------------------------------------------------------
SoundBuffer::Ptr FeatureSoundBufferFactory::m_SoundBuffer = nullptr;

//----------------------------------------------------------------------------
SoundBuffer::Ptr FeatureSoundBufferFactory::Create() const
{
    if (m_SoundBuffer == nullptr)
    {
        SoundBuffer* soundBuffer = new SoundBuffer("assets/feature_complete.wav");
        soundBuffer->SetDataNameId(SoundBuffers::k_Feature);

        m_SoundBuffer.reset(soundBuffer);
    }

    return m_SoundBuffer;
}

//----------------------------------------------------------------------------
SoundBuffer::Ptr DeathSoundBufferFactory::m_SoundBuffer = nullptr;

//----------------------------------------------------------------------------
SoundBuffer::Ptr DeathSoundBufferFactory::Create() const
{
    if (m_SoundBuffer == nullptr)
    {
        SoundBuffer* soundBuffer = new SoundBuffer("assets/death.wav");
        soundBuffer->SetDataNameId(SoundBuffers::k_Death);

        m_SoundBuffer.reset(soundBuffer);
    }

    return m_SoundBuffer;
}

//----------------------------------------------------------------------------
SoundBuffer::Ptr KeyboardSoundBufferFactory::m_SoundBuffer = nullptr;

//----------------------------------------------------------------------------
SoundBuffer::Ptr KeyboardSoundBufferFactory::Create() const
{
    if (m_SoundBuffer == nullptr)
    {
        SoundBuffer* soundBuffer = new SoundBuffer("assets/keyboard.wav");
        soundBuffer->SetDataNameId(SoundBuffers::k_Keyboard);

        m_SoundBuffer.reset(soundBuffer);
    }

    return m_SoundBuffer;
}