#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/audio/Sound.h>

#include <data/audio/SoundFactories.h>

//----------------------------------------------------------------------------
class SoundCatalog : public DataCatalog<Sound>
{
    forge_DeclCatalog(SoundCatalog);

public:
    SoundCatalog();
    ~SoundCatalog();

private:
    WhipSoundFactory     m_WhipSoundFactory;
    CoinSoundFactory     m_CoinSoundFactory;
    DeathSoundFactory    m_DeathSoundFactory;
    FeatureSoundFactory  m_FeatureSoundFactory;
    KeyboardSoundFactory m_KeyboardSoundFactory;
};
