#include <data/audio/MusicCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
MusicCatalog::MusicCatalog()
{
    RegisterData(Musics::k_FunkyLoop, m_FunkyLoopMusicFactory);
}

//----------------------------------------------------------------------------
MusicCatalog::~MusicCatalog()
{
    m_Factories.clear();
}
