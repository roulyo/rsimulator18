#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/audio/Sound.h>

//----------------------------------------------------------------------------
class WhipSoundFactory : public AbstractDataFactory
{
public:
    Sound::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class CoinSoundFactory : public AbstractDataFactory
{
public:
    Sound::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class FeatureSoundFactory : public AbstractDataFactory
{
public:
    Sound::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class DeathSoundFactory : public AbstractDataFactory
{
public:
    Sound::Ptr Create() const override;
};

//----------------------------------------------------------------------------
class KeyboardSoundFactory : public AbstractDataFactory
{
public:
    Sound::Ptr Create() const override;
};
