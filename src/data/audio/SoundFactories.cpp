#include <data/audio/SoundFactories.h>

#include <data/audio/SoundBufferCatalog.h>

#include <data/DataNameIds.h>

#include <forge/system/data/DataManager.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
Sound::Ptr WhipSoundFactory::Create() const
{
    const SoundBufferCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundBufferCatalog>();

    Sound* sound = nullptr;

    if (catalog != nullptr)
    {
        sound = new Sound(catalog->Get(SoundBuffers::k_Whip));
        sound->SetDataNameId(Sounds::k_Whip);
    }

    return Sound::Ptr(sound);
}

//----------------------------------------------------------------------------
Sound::Ptr CoinSoundFactory::Create() const
{
    const SoundBufferCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundBufferCatalog>();

    Sound* sound = nullptr;

    if (catalog != nullptr)
    {
        sound = new Sound(catalog->Get(SoundBuffers::k_Coin));
        sound->SetDataNameId(Sounds::k_Coin);
    }

    return Sound::Ptr(sound);
}

//----------------------------------------------------------------------------
Sound::Ptr FeatureSoundFactory::Create() const
{
    const SoundBufferCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundBufferCatalog>();

    Sound* sound = nullptr;

    if (catalog != nullptr)
    {
        sound = new Sound(catalog->Get(SoundBuffers::k_Feature));
        sound->SetDataNameId(Sounds::k_Feature);
    }

    return Sound::Ptr(sound);
}

//----------------------------------------------------------------------------
Sound::Ptr DeathSoundFactory::Create() const
{
    const SoundBufferCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundBufferCatalog>();

    Sound* sound = nullptr;

    if (catalog != nullptr)
    {
        sound = new Sound(catalog->Get(SoundBuffers::k_Death));
        sound->SetDataNameId(Sounds::k_Death);
    }

    return Sound::Ptr(sound);
}

//----------------------------------------------------------------------------
Sound::Ptr KeyboardSoundFactory::Create() const
{
    const SoundBufferCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundBufferCatalog>();

    Sound* sound = nullptr;

    if (catalog != nullptr)
    {
        sound = new Sound(catalog->Get(SoundBuffers::k_Keyboard));
        sound->SetDataNameId(Sounds::k_Keyboard);
    }

    return Sound::Ptr(sound);
}
