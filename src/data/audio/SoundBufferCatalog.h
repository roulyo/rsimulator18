#pragma once

#include <forge/system/data/DataCatalog.h>

#include <forge/audio/SoundBuffer.h>

#include <data/audio/SoundBufferFactories.h>

//----------------------------------------------------------------------------
class SoundBufferCatalog : public DataCatalog<SoundBuffer>
{
    forge_DeclCatalog(SoundBufferCatalog);

public:
    SoundBufferCatalog();
    ~SoundBufferCatalog();

private:
    WhipSoundBufferFactory      m_WhipSoundBufferFactory;
    CoinSoundBufferFactory      m_CoinSoundBufferFactory;
    FeatureSoundBufferFactory   m_FeatureSoundBufferFactory;
    DeathSoundBufferFactory     m_DeathSoundBufferFactory;
    KeyboardSoundBufferFactory  m_KeyboardSoundBufferFactory;
};
