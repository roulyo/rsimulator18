#include <data/audio/SoundBufferCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
SoundBufferCatalog::SoundBufferCatalog()
{
    RegisterData(SoundBuffers::k_Whip,      m_WhipSoundBufferFactory);
    RegisterData(SoundBuffers::k_Coin,      m_CoinSoundBufferFactory);
    RegisterData(SoundBuffers::k_Feature,   m_FeatureSoundBufferFactory);
    RegisterData(SoundBuffers::k_Death,     m_DeathSoundBufferFactory);
    RegisterData(SoundBuffers::k_Keyboard,  m_KeyboardSoundBufferFactory);
}

//----------------------------------------------------------------------------
SoundBufferCatalog::~SoundBufferCatalog()
{
    m_Factories.clear();
}
