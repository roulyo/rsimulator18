#pragma once

#include <forge/system/data/DataFactory.h>

#include <forge/audio/SoundBuffer.h>

//----------------------------------------------------------------------------
class WhipSoundBufferFactory : public AbstractDataFactory
{
public:
    SoundBuffer::Ptr Create() const override;

private:
    static SoundBuffer::Ptr m_SoundBuffer;

};

//----------------------------------------------------------------------------
class CoinSoundBufferFactory : public AbstractDataFactory
{
public:
    SoundBuffer::Ptr Create() const override;

private:
    static SoundBuffer::Ptr m_SoundBuffer;

};

//----------------------------------------------------------------------------
class FeatureSoundBufferFactory : public AbstractDataFactory
{
public:
    SoundBuffer::Ptr Create() const override;

private:
    static SoundBuffer::Ptr m_SoundBuffer;

};

//----------------------------------------------------------------------------
class DeathSoundBufferFactory : public AbstractDataFactory
{
public:
    SoundBuffer::Ptr Create() const override;

private:
    static SoundBuffer::Ptr m_SoundBuffer;

};

//----------------------------------------------------------------------------
class KeyboardSoundBufferFactory : public AbstractDataFactory
{
public:
    SoundBuffer::Ptr Create() const override;

private:
    static SoundBuffer::Ptr m_SoundBuffer;

};
