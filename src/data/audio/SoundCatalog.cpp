#include <data/audio/SoundCatalog.h>

#include <data/DataNameIds.h>

using namespace DataNameIds;

//----------------------------------------------------------------------------
SoundCatalog::SoundCatalog()
{
    RegisterData(Sounds::k_Whip,     m_WhipSoundFactory);
    RegisterData(Sounds::k_Coin,     m_CoinSoundFactory);
    RegisterData(Sounds::k_Feature,  m_FeatureSoundFactory);
    RegisterData(Sounds::k_Death,    m_DeathSoundFactory);
    RegisterData(Sounds::k_Keyboard, m_KeyboardSoundFactory);
}

//----------------------------------------------------------------------------
SoundCatalog::~SoundCatalog()
{
    m_Factories.clear();
}
