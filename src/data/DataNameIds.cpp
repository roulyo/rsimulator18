#include <data/DataNameIds.h>

namespace DataNameIds
{
    // Textures
    namespace Textures
    {
        constexpr DataNameId k_FloorSpriteSheet     = DataNameId("Textures::k_FloorSpriteSheet");
        constexpr DataNameId k_DefaultSpriteSheet   = DataNameId("Textures::k_DefaultSpriteSheet");
        constexpr DataNameId k_Table                = DataNameId("Textures::k_Table");
        constexpr DataNameId k_Icons                = DataNameId("Textures::k_Icons");
        constexpr DataNameId k_SplashScreen         = DataNameId("Textures::k_SplashScreen");
        constexpr DataNameId k_ScoreScreen          = DataNameId("Textures::k_ScoreScreen");
        constexpr DataNameId k_ScoreBackground      = DataNameId("Textures::k_ScoreBackground");
        
    }

    // Animations
    namespace Animations
    {
        constexpr DataNameId k_IdleFront    = DataNameId("Animations::k_IdleFront");
        constexpr DataNameId k_IdleBack     = DataNameId("Animations::k_IdleBack");
        constexpr DataNameId k_FXAnim       = DataNameId("Animations::k_FXAnim");
    }

    // Sprites
    namespace Sprites
    {
        constexpr DataNameId k_Character        = DataNameId("Sprites::k_Character");
        constexpr DataNameId k_Coin             = DataNameId("Sprites::k_Coin");
        constexpr DataNameId k_Whip             = DataNameId("Sprites::k_Whip");
        constexpr DataNameId k_Floor            = DataNameId("Sprites::k_FloorSpriteSheet");
        constexpr DataNameId k_Wall             = DataNameId("Sprites::k_WallSpriteSheet");
        constexpr DataNameId k_CharacterGreen   = DataNameId("Sprites::k_CharacterGreen");
        constexpr DataNameId k_CharacterRed     = DataNameId("Sprites::k_CharacterRed");
        constexpr DataNameId k_CharacterBlue    = DataNameId("Sprites::k_CharacterBlue");
        constexpr DataNameId k_Table            = DataNameId("Sprites::k_Table");
        constexpr DataNameId k_ArtIcon          = DataNameId("Textures::k_ArtIcon");
        constexpr DataNameId k_CodeIcon         = DataNameId("Textures::k_CodeIcon");
        constexpr DataNameId k_DesignIcon       = DataNameId("Textures::k_DesignIcon");
        constexpr DataNameId k_SplashScreen     = DataNameId("Sprites::k_SplashScreen");
        constexpr DataNameId k_ScoreScreen      = DataNameId("Sprites::k_ScoreScreen");
        constexpr DataNameId k_ScoreBackground  = DataNameId("Sprites::k_ScoreBackground");
    }

    // Entities
    namespace Entities
    {
        constexpr DataNameId k_Character    = DataNameId("Entities::k_Character");
        constexpr DataNameId k_Worker       = DataNameId("Entities::k_Worker");
        constexpr DataNameId k_Floor        = DataNameId("Entities::k_FloorSpriteSheet");
        constexpr DataNameId k_Table        = DataNameId("Entities::k_Table");
        constexpr DataNameId k_SplashScreen = DataNameId("Entities::k_SplashScreen");
    }
    
    // SoundBuffers
    namespace SoundBuffers
    {
        constexpr DataNameId k_Whip         = DataNameId("SoundBuffers::k_Whip");
        constexpr DataNameId k_Coin         = DataNameId("SoundBuffers::k_Coin");
        constexpr DataNameId k_Feature      = DataNameId("SoundBuffers::k_Feature");
        constexpr DataNameId k_Death        = DataNameId("SoundBuffers::k_Death");
        constexpr DataNameId k_Keyboard     = DataNameId("SoundBuffers::k_Keyboard");
    }

    // Sounds
    namespace Sounds
    {
        constexpr DataNameId k_Whip         = DataNameId("Sounds::k_Whip");
        constexpr DataNameId k_Coin         = DataNameId("Sounds::k_Coin");
        constexpr DataNameId k_Feature      = DataNameId("Sounds::k_Feature");
        constexpr DataNameId k_Death        = DataNameId("Sounds::k_Death");
        constexpr DataNameId k_Keyboard     = DataNameId("Sounds::k_Keyboard");
    }

    // Musics
    namespace Musics
    {
        constexpr DataNameId k_FunkyLoop    = DataNameId("Musics::k_FunkyLoop");
    }

    // Fonts
    namespace Fonts
    {
        constexpr DataNameId k_Pixelli      = DataNameId("Fonts::k_Pixelli");
        constexpr DataNameId k_Number       = DataNameId("Fonts::k_Number");
        constexpr DataNameId k_SansSerif    = DataNameId("Fonts::k_SansSerif");
    }

    // FXs
    namespace FXs
    {
        constexpr DataNameId k_Whip         = DataNameId("FXs::k_Whip");
        constexpr DataNameId k_Coin         = DataNameId("FXs::k_Coin");
    }
}
