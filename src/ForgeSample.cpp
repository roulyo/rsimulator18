#include <ForgeSample.h>

#include <forge/system/data/DataManager.h>
#include <forge/audio/AudioManager.h>

#include <data/animations/AnimationCatalog.h>
#include <data/entities/EntityCatalog.h>
#include <data/sprites/SpriteCatalog.h>
#include <data/textures/TextureCatalog.h>
#include <data/audio/MusicCatalog.h>
#include <data/audio/SoundBufferCatalog.h>
#include <data/audio/SoundCatalog.h>
#include <data/fonts/FontCatalog.h>
#include <data/fxs/FXCatalog.h>
#include <data/DataNameIds.h>

#include <events/game/RestartGameEvent.h>

#include <components/RenderableComponent.h>

#include <Global.h>
#include <gamestates/GameDirector.h>


//----------------------------------------------------------------------------
ForgeSample::ForgeSample()
    : m_MainGameState(*this)
    , m_ScoreState(*this)
    , m_StartScreenState(*this)
{
    m_GameDirector = new GameDirector();
}

//----------------------------------------------------------------------------
ForgeSample::~ForgeSample()
{
    delete m_GameDirector;
}

//----------------------------------------------------------------------------
void ForgeSample::Init(const std::string& _gameName)
{
    AbstractForgeGame::Init(_gameName);

    DataManager::GetInstance().RegisterCatalog<AnimationCatalog>();
    DataManager::GetInstance().RegisterCatalog<EntityCatalog>();
    DataManager::GetInstance().RegisterCatalog<SpriteCatalog>();
    DataManager::GetInstance().RegisterCatalog<TextureCatalog>();
    DataManager::GetInstance().RegisterCatalog<MusicCatalog>();
    DataManager::GetInstance().RegisterCatalog<SoundBufferCatalog>();
    DataManager::GetInstance().RegisterCatalog<SoundCatalog>();
    DataManager::GetInstance().RegisterCatalog<FontCatalog>();
    DataManager::GetInstance().RegisterCatalog<FXCatalog>();

    PushState(m_StartScreenState);
    
    InitStartWorld();
	
	const MusicCatalog* catalog = DataManager::GetInstance().GetCatalog<MusicCatalog>();

    if (catalog)
    {
        bool isLooping = true;
        Music::Ptr funkyLoop = catalog->Get(DataNameIds::Musics::k_FunkyLoop);
        
        AudioManager::GetInstance().PlayMusic(funkyLoop, isLooping);
    }

    m_GameDirector->Init();

    RestartGameEvent::Handlers += RestartGameEvent::Handler(this, &ForgeSample::OnRestartGameEvent);
}

//----------------------------------------------------------------------------
void ForgeSample::Quit()
{
    RestartGameEvent::Handlers -= RestartGameEvent::Handler(this, &ForgeSample::OnRestartGameEvent);
}

//----------------------------------------------------------------------------
void ForgeSample::Update(const long long& _dt)
{
    AbstractForgeGame::Update(_dt);

    if (GetTopState() != &m_StartScreenState)    
    {
        m_GameDirector->Update(_dt); 
        if (m_GameDirector->IsGameOver() && GetTopState() != &m_ScoreState)
        {
            PopState();
            PushState(m_ScoreState);
        }
    }    
}

//----------------------------------------------------------------------------
void ForgeSample::OnRestartGameEvent(const RestartGameEvent& _event)
{
    ResetGameWorld();
    PopState();
    PushState(m_MainGameState);
}

//----------------------------------------------------------------------------
void ForgeSample::InitStartWorld()
{
    m_World.Init(2000, 2000);

    AddSplashScreenEntity();
}

//----------------------------------------------------------------------------
void ForgeSample::ChangeLight(unsigned short _intensity)
{
    std::vector<Entity::Ptr> entities;
    AABB aabb(0, 0, 2000, 2000);
    m_World.GetEntitiesInRange(aabb, entities);

    for (Entity::Ptr entity : entities)
    {
        RenderableComponent& renderableComp = entity->As<Entity>()->GetComponent<RenderableComponent>();
        renderableComp.GetSprite().SetOverlayColor(_intensity, _intensity, _intensity);
    }
}

//----------------------------------------------------------------------------
void ForgeSample::InitGameWorld()
{
    m_World.Init(2000, 2000);

    AddBackgroundAssets();

    AddWorkerGroup(200.0f, 340.0f);
    AddWorkerGroup(550.0f, 200.0f);
    AddWorkerGroup(550.0f, 500.0f);
}

//----------------------------------------------------------------------------
void ForgeSample::AddWorkerGroup(float _x, float _y)
{
    // Add workers
    const EntityCatalog* catalog = DataManager::GetInstance().GetCatalog<EntityCatalog>();

    if (catalog != nullptr)
    {
        for (size_t i = 0; i < Global::workers.size(); ++i)
        {
            Entity::Ptr workerPtr = catalog->Get(DataNameIds::Entities::k_Worker);
            Entity* worker = workerPtr->As<Entity>();

            worker->SetPosition(Global::workers[i].positionX + _x, Global::workers[i].positionY + _y);

            RenderableComponent& renderComp = worker->GetComponent<RenderableComponent>();

            if (i < 3)
            {
                renderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_IdleFront);
                renderComp.GetSprite().SetZValue(1);
            }
            else
            {
                renderComp.GetSprite().PlayAnimation(DataNameIds::Animations::k_IdleBack);
                renderComp.GetSprite().SetZValue(3);
            }

            m_World.AddEntity(workerPtr);
        }
    }

    // Add table
    Entity::Ptr tablePtr = catalog->Get(DataNameIds::Entities::k_Table);
    Entity* table = tablePtr->As<Entity>();
    RenderableComponent& tableRenderComp = table->GetComponent<RenderableComponent>();

    table->SetPosition(_x - 95.0f, _y - 50.0f);
    tableRenderComp.GetSprite().SetZValue(2);
    m_World.AddEntity(tablePtr);
}

//----------------------------------------------------------------------------
void ForgeSample::ResetGameWorld()
{
    m_World.Destroy();
    InitGameWorld();
}

//----------------------------------------------------------------------------
void ForgeSample::AddBackgroundAssets()
{
    const EntityCatalog* catalog = DataManager::GetInstance().GetCatalog<EntityCatalog>();

    if (catalog != nullptr)
    {
        Entity::Ptr floor = catalog->Get(DataNameIds::Entities::k_Floor);
        floor->As<Entity>()->SetPosition(0, 0);
        m_World.AddEntity(floor);
    }
}

//----------------------------------------------------------------------------
void ForgeSample::AddSplashScreenEntity()
{
    const EntityCatalog* catalog = DataManager::GetInstance().GetCatalog<EntityCatalog>();

    if (catalog != nullptr)
    {
        Entity::Ptr splashScreen = catalog->Get(DataNameIds::Entities::k_SplashScreen);
        splashScreen->As<Entity>()->SetPosition(0, 0);
        m_World.AddEntity(splashScreen);
    }
}