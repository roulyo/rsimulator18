#pragma once
#include <forge/system/data/DataFactory.h>
#include <vector>
#include <string>

//----------------------------------------------------------------------------
enum Skill
{
    Art,
    Code,
    Design,

    Skills_Count
};

struct Global
{
    //----------------------------------------------------------------------------
    static const int NB_FEATURES = 10;
    static const int NB_STARTING_WORKERS;    
    static const int MAX_WORKER_SKILL_SCORE;
    static const float WORKER_SALARY;
    static const float GAME_DURATION; 
    static const float AVAILABLE_MONEY;
    static const float STARTING_STRESS_LEVEL;
    static const float MONEY_BONUS_BY_PATPAT;

    static const int STRESS_UP_BY_WHIP;
    static const int STRESS_DOWN_BY_PATPAT;

    static const float STATS_UP_BY_WHIP;
    static const float STATS_UP_BY_PATPAT;

    static const float PATPAT_STAT_BOOST_DURATION;
    static const float WHIPPED_STAT_BOOST_DURATION;
    static const float STRESS_LOST_BY_SECOND;
    static const float CHECK_FOR_QUIT_TIMER;

    static const float STRESS_FOR_MAX_RED_FACE;
    static const float MIN_FEATURE_COST;
    static const float MAX_FEATURE_COST;

    //----------------------------------------------------------------------------
    static std::string FeatureTypeToString(Skill type);

    //----------------------------------------------------------------------------
    static void ResetRandomSeed();
    
    //----------------------------------------------------------------------------
    struct position
    {
        float positionX;
        float positionY;
    };
    
    static constexpr position worker1 = { -81.0, -60.0 };
    static constexpr position worker2 = {  00.0, -60.0 };
    static constexpr position worker3 = { +85.0, -60.0 };
    static constexpr position worker4 = { -81.0,  50.0 };
    static constexpr position worker5 = {  00.0,  50.0 };
    static constexpr position worker6 = { +85.0,  50.0 };

    static std::vector <position> workers;

    static std::vector <std::string> artFeatures;
    static std::vector <std::string> codeFeatures;
    static std::vector <std::string> designFeatures;

    static std::vector <std::string> buzzWords;
    
    static std::vector <std::string> startGameName;
    static std::vector <std::string> endGameName;
    static std::vector <std::string> followGameName;
};