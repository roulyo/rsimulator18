#pragma once

#include <forge/2d/drawable/Text.h>
#include <forge/core/ecs/Component.h>

#include <events/inputs/MouseEvent.h>

#include <Global.h>


//----------------------------------------------------------------------------
class WorkerComponent : public AbstractComponent
{
    forge_DeclComponent(WorkerComponent);

public:
    WorkerComponent();
    ~WorkerComponent();

    void OnWorkerClicked(MouseButton _button);
    void SetStressLevel(float _stressLevel);
    float GetStressLevel();
	
    float GetSkillScore(Skill skill);
    void UpdateStressLevel(const long long& _dt);
    void UpdateEffectsDurations(const long long& _dt, float _x, float _y, bool _isFrontWorker);
    void CheckForQuit();

    bool HasQuit() const;
    
private:
    void OnWorkerWhipped();
    void OnWorkerPatPat();

    void DebugLog();

    float m_WhipBoostTimer;
    float m_PatPatBoostTimer;

    float  m_stressLevel;
    float  m_salary;
    int    m_NbCurrentWhipEffects;
    int    m_NbCurrentPatPatEffects;

    float  m_SkillScores[Skill::Skills_Count];
    bool   m_HasQuit;

    Font::Ptr  m_Font;
    Text       m_StatText[Skill::Skills_Count];

};