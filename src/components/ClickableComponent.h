#pragma once

#include <forge/core/ecs/Component.h>

//----------------------------------------------------------------------------
class ClickableComponent : public AbstractComponent
{
    forge_DeclComponent(ClickableComponent);    
};