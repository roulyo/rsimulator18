#include <components/WorkerComponent.h>

#include <forge/audio/AudioManager.h>
#include <forge/system/data/DataManager.h>
#include <forge/system/window/WindowManager.h>

#include <data/audio/SoundCatalog.h>
#include <data/fonts/FontCatalog.h>
#include <data/DataNameIds.h>

#include <events/game/MoneyBonusEvent.h>
#include <events/game/WorkerQuitEvent.h>

//----------------------------------------------------------------------------
forge_DefComponent(WorkerComponent);

//----------------------------------------------------------------------------
WorkerComponent::WorkerComponent()
    : m_stressLevel(Global::STARTING_STRESS_LEVEL)
    , m_salary(Global::WORKER_SALARY)
    , m_NbCurrentWhipEffects(0)
    , m_NbCurrentPatPatEffects(0)
    , m_PatPatBoostTimer(0.0f)
    , m_WhipBoostTimer(0.0f)
    , m_HasQuit(false)
{   
    for (size_t i = 0; i < Skill::Skills_Count; ++i)
    {
        m_SkillScores[i] = static_cast<float>(rand() % Global::MAX_WORKER_SKILL_SCORE);
    }

    if (const FontCatalog* catalog = DataManager::GetInstance().GetCatalog<FontCatalog>())
    {
        m_Font = catalog->Get(DataNameIds::Fonts::k_Pixelli);

         for (size_t i = 0; i < Skill::Skills_Count; ++i)
         {
             m_StatText[i].SetFont(m_Font);
             m_StatText[i].SetZValue(1000);
         }
    }

    DebugLog();
}

//----------------------------------------------------------------------------
WorkerComponent::~WorkerComponent()
{
    
}

//----------------------------------------------------------------------------
void WorkerComponent::OnWorkerClicked(MouseButton _button)
{
    if (m_HasQuit)
    {
        return;
    }

    switch (_button)
    {
    case MouseButton::Left: OnWorkerWhipped(); break;
    case MouseButton::Right: OnWorkerPatPat(); break;
    }
}

//----------------------------------------------------------------------------
void WorkerComponent::OnWorkerWhipped()
{
    SetStressLevel(m_stressLevel + Global::STRESS_UP_BY_WHIP);

    m_NbCurrentWhipEffects++;
    m_WhipBoostTimer = Global::WHIPPED_STAT_BOOST_DURATION;

    DebugLog();
}

//----------------------------------------------------------------------------
void WorkerComponent::OnWorkerPatPat()
{
    SetStressLevel(m_stressLevel - Global::STRESS_DOWN_BY_PATPAT);

    m_NbCurrentPatPatEffects++;
    m_PatPatBoostTimer = Global::PATPAT_STAT_BOOST_DURATION;

    DebugLog();

    MoneyBonusEvent::Ptr bonusEvent(new MoneyBonusEvent(Global::MONEY_BONUS_BY_PATPAT));
    EventManager::GetInstance().Push(bonusEvent);
}

//----------------------------------------------------------------------------
void WorkerComponent::SetStressLevel(float _stressLevel)
{
    if (_stressLevel <= 0.0f)
    {
        m_stressLevel = 0.0f;
    }
    else
    {
        m_stressLevel = _stressLevel;
    }
}

//----------------------------------------------------------------------------
void WorkerComponent::DebugLog()
{
    /*std::cout << "Worker infos: " << std::endl;

    for (size_t i = 0; i < Skill::Skills_Count; ++i)
    {
        std::cout << Global::FeatureTypeToString(static_cast<Skill>(i)) << ": " << m_SkillScores[i] << std::endl;
    }

    std::cout << "PatPat Bonus: " << m_NbCurrentPatPatEffects * Global::STATS_UP_BY_PATPAT << std::endl;
    std::cout << "Whip Bonus: " << m_NbCurrentWhipEffects * Global::STATS_UP_BY_WHIP << std::endl;

    std::cout << "Salary: " << m_salary << std::endl;
    std::cout << "StressLevel: " << m_stressLevel << std::endl;

    std::cout << std::endl;*/
}

//----------------------------------------------------------------------------
float WorkerComponent::GetStressLevel()
{
    return m_stressLevel;
}

//----------------------------------------------------------------------------
float WorkerComponent::GetSkillScore(Skill skill)
{
    if (m_HasQuit)
    {
        return 0.0f;
    }

    float patPatBonus = m_NbCurrentPatPatEffects * Global::STATS_UP_BY_PATPAT;
    float whipBonus = m_NbCurrentWhipEffects * Global::STATS_UP_BY_WHIP;
    return static_cast<float>(m_SkillScores[skill]) * (1.0f + patPatBonus + whipBonus);
}

//----------------------------------------------------------------------------
void WorkerComponent::UpdateStressLevel(const long long& _dt)
{
    if (m_stressLevel > 0.0f)
    {
        SetStressLevel(m_stressLevel - (Global::STRESS_LOST_BY_SECOND * _dt) / 1000.0f);
    }
}

//----------------------------------------------------------------------------
void WorkerComponent::UpdateEffectsDurations(const long long& _dt, float _x, float _y, bool _isFrontWorker)
{
    for (size_t i = 0; i < Skill::Skills_Count; ++i)
    {
        Skill currentSkill = static_cast<Skill>(i);
        int score = static_cast<int>(GetSkillScore(currentSkill));

        if (_isFrontWorker)
        {
            m_StatText[i].SetScreenCoord(_x - 2.0f + i * 20.0f, _y - 30.0f);
        }
        else
        {
            m_StatText[i].SetScreenCoord(_x - 2.0f + i * 20.0f, _y + 10.0f);
        }

        m_StatText[i].SetString(std::to_string(score));
        switch (currentSkill)
        {
        case Art:
            m_StatText[i].SetFillColor(255, 0, 0);
            break;
        case Code:
            m_StatText[i].SetFillColor(0, 255, 0);
            break;
        case Design:
            m_StatText[i].SetFillColor(0, 0, 255);
            break;
        }
        WindowManager::GetInstance().PushToRender(m_StatText[i].GetRenderingData());
    }

    if (m_NbCurrentPatPatEffects > 0)
    {
        m_PatPatBoostTimer -= _dt / 1000.0f;
        if (m_PatPatBoostTimer < 0.0f)
        {
            m_NbCurrentPatPatEffects = 0;
        }
    }

    if (m_NbCurrentWhipEffects > 0)
    {
        m_WhipBoostTimer -= _dt / 1000.0f;
        if (m_WhipBoostTimer < 0.0f)
        {
            m_NbCurrentWhipEffects = 0;
        }
    }
}

//----------------------------------------------------------------------------
void WorkerComponent::CheckForQuit()
{
    if (!m_HasQuit)
    {
        if ((rand() % 100) < (m_stressLevel / 10.0f))
        {
            const SoundCatalog* catalog = DataManager::GetInstance().GetCatalog<SoundCatalog>();

            if (catalog)
            {
                Sound::Ptr deathSound = catalog->Get(DataNameIds::Sounds::k_Death);
                AudioManager::GetInstance().PlaySound(deathSound);
            }
            
            m_HasQuit = true;

            WorkerQuitEvent::Ptr quitEvent(new WorkerQuitEvent());
            EventManager::GetInstance().Push(quitEvent);
        }
    }
}


//----------------------------------------------------------------------------
bool WorkerComponent::HasQuit() const
{
    return m_HasQuit;
}
