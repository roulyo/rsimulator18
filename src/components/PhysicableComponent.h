#pragma once

#include <forge/core/ecs/Component.h>

//----------------------------------------------------------------------------
class PhysicableComponent : public AbstractComponent
{
    forge_DeclComponent(PhysicableComponent);

public:
    bool    IsBlocking;

    float   SpeedX;
    float   SpeedY;

};
