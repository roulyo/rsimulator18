#pragma once

#include <forge/core/ecs/Component.h>

//----------------------------------------------------------------------------
class PlayableCharacterComponent : public AbstractComponent
{
    forge_DeclComponent(PlayableCharacterComponent);
};