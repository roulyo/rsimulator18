#pragma once

#include <forge/core/ecs/System.h>

#include <events/inputs/MouseEvent.h>

#include <components/ClickableComponent.h>
#include <components/RenderableComponent.h>
#include <components/WorkerComponent.h>
#include <Global.h>

//----------------------------------------------------------------------------
class MouseEvent;

//----------------------------------------------------------------------------
class ClickableWorkerSystem : public System<ClickableComponent, WorkerComponent, RenderableComponent>
{
public:
    ClickableWorkerSystem();
    ~ClickableWorkerSystem();

    void Start() override;
    void Stop() override;

    void PreExecute() override;
    void Execute(const long long& _dt, Entity& _entity) const override;
    void PostExecute() override;

    void OnMouseClickEvent(const MouseClickEvent& _event);

private:
    void PlayFX(const Entity& _entity, MouseButton _button) const;

    struct MouseClickData
    {
        bool m_IsPressed = false;
        unsigned short m_X = 0;
        unsigned short m_Y = 0;
        MouseButton m_Button = MouseButton::Left;
    };

    std::vector<MouseClickData> m_MouseClickReceived;

   mutable float m_TotalSkillValues[Skill::Skills_Count];


};
