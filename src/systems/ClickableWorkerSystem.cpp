#include <systems/ClickableWorkerSystem.h>

#include <forge/system/window/WindowManager.h>

#include <forge/2d/camera/CameraManager.h>
#include <events/game/WorkersJobDoneEvent.h>

#include <forge/system/data/DataManager.h>
#include <forge/core/fx/FXManager.h>
#include <data/fxs/FXCatalog.h>
#include <data/DataNameIds.h>

//----------------------------------------------------------------------------
ClickableWorkerSystem::ClickableWorkerSystem()
{
    m_MouseClickReceived.reserve(10);
}

//----------------------------------------------------------------------------
ClickableWorkerSystem::~ClickableWorkerSystem()
{

}

//----------------------------------------------------------------------------
void ClickableWorkerSystem::Start()
{
    MouseClickEvent::Handlers += MouseClickEvent::Handler(this, &ClickableWorkerSystem::OnMouseClickEvent);
}

//----------------------------------------------------------------------------
void ClickableWorkerSystem::Stop()
{
    MouseClickEvent::Handlers -= MouseClickEvent::Handler(this, &ClickableWorkerSystem::OnMouseClickEvent);
}

//----------------------------------------------------------------------------
void ClickableWorkerSystem::PreExecute()
{
    for (int i = 0; i < Skill::Skills_Count; ++i)
    {
        m_TotalSkillValues[i] = 0;
    }
}

//----------------------------------------------------------------------------
void ClickableWorkerSystem::Execute(const long long& _dt, Entity& _entity) const
{
    WorkerComponent& workerComp = _entity.GetComponent<WorkerComponent>();

    if (workerComp.HasQuit())
    {
        return;
    }

    for (const MouseClickData& clickData : m_MouseClickReceived)
    {
        float x = static_cast<float>(clickData.m_X);
        float y = static_cast<float>(clickData.m_Y);
        if (_entity.GetAABB().contains(x, y))
        {
            if (!clickData.m_IsPressed)
            {
                PlayFX(_entity, clickData.m_Button);
                workerComp.OnWorkerClicked(clickData.m_Button);
            }
        }       
    }

    for (int i = 0; i < Skill::Skills_Count; ++i)
    {
        m_TotalSkillValues[i] += workerComp.GetSkillScore(static_cast<Skill>(i));
    }

    
    workerComp.UpdateStressLevel(_dt);

    RenderableComponent& renderComp = _entity.GetComponent<RenderableComponent>();
    bool isFront = (renderComp.GetSprite().GetZValue() == 1);
    workerComp.UpdateEffectsDurations(_dt, _entity.GetAABB().left, _entity.GetAABB().top, isFront);
}

//----------------------------------------------------------------------------
void ClickableWorkerSystem::PostExecute()
{
    for (int i = 0; i < Skill::Skills_Count; ++i)
    {
        WorkersJobDoneEvent::Ptr workerEvent(new WorkersJobDoneEvent(static_cast<Skill>(i), m_TotalSkillValues[i]));
        EventManager::GetInstance().Push(workerEvent);
    }

    m_MouseClickReceived.clear();
}

//----------------------------------------------------------------------------
void ClickableWorkerSystem::OnMouseClickEvent(const MouseClickEvent& _event)
{
    MouseClickData clickData;
    clickData.m_IsPressed = _event.GetIsPressed();
    clickData.m_X = _event.GetX();
    clickData.m_Y = _event.GetY();
    clickData.m_Button = _event.GetMouseButton();
    m_MouseClickReceived.push_back(clickData);    
}

//----------------------------------------------------------------------------
void ClickableWorkerSystem::PlayFX(const Entity& _entity, MouseButton _button) const
{
    const FXCatalog* catalog = DataManager::GetInstance().GetCatalog<FXCatalog>();

    if (catalog != nullptr)
    {
        float spriteSize = 32.0f;
        float halfSpriteSize = spriteSize / 2.0f;

        if (_button == MouseButton::Left)
        {
            sf::Vector2f coord(_entity.GetAABB().left - spriteSize - halfSpriteSize, _entity.GetAABB().top - spriteSize - halfSpriteSize);
            FX::Ptr fx = catalog->Get(DataNameIds::FXs::k_Whip);
            FXManager::GetInstance().PlayFX(coord, fx);
        }
        else if (_button == MouseButton::Right)
        {
            sf::Vector2f coord(_entity.GetAABB().left + halfSpriteSize, _entity.GetAABB().top - spriteSize - halfSpriteSize);
            FX::Ptr fx = catalog->Get(DataNameIds::FXs::k_Coin);
            FXManager::GetInstance().PlayFX(coord, fx);
        }
    }
}