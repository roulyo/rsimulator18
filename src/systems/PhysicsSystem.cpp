#include <systems/PhysicsSystem.h>

//----------------------------------------------------------------------------
void PhysicsSystem::Execute(const long long& _dt, Entity& _entity) const
{
    PhysicableComponent& physicComp = _entity.GetComponent<PhysicableComponent>();

    float currentX = _entity.GetAABB().left;
    float currentY = _entity.GetAABB().top;

    float deltaX = (physicComp.SpeedX * k_UnitPerMeter * _dt) / 1000.0f;
    float deltaY = (physicComp.SpeedY * k_UnitPerMeter * _dt) / 1000.0f;

    if (deltaX != 0 || deltaY != 0)
    {
        _entity.SetPosition(currentX + deltaX, currentY + deltaY);
    }
}
