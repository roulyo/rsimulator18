#pragma once

#include <forge/core/ecs/System.h>

#include <components/RenderableComponent.h>

//----------------------------------------------------------------------------
class RenderingSystem : public System<RenderableComponent>
{
public:
    void Execute(const long long& _dt, Entity& _entity) const override;

};
