#include <systems/StressLevelSystem.h>
#include <Global.h>

#include <events/game/WorkerTotalStressUpdateEvent.h>

//----------------------------------------------------------------------------
void StressLevelSystem::PreExecute()
{
    m_TotalStressLevel = 0.0f;
}

//----------------------------------------------------------------------------
void StressLevelSystem::Execute(const long long& _dt, Entity& _entity) const
{
    WorkerComponent& workerComp = _entity.GetComponent<WorkerComponent>();
    RenderableComponent& renderComp = _entity.GetComponent<RenderableComponent>();
    
    if (m_QuitTimer.getElapsedTime().asSeconds() > Global::CHECK_FOR_QUIT_TIMER)
    {
        workerComp.CheckForQuit();
    }

    if (workerComp.HasQuit())
    {
        renderComp.GetSprite().SetOverlayColor(15, 15, 15);
    }
    else
    {
        float stressLevel = workerComp.GetStressLevel();
        m_TotalStressLevel += stressLevel;
        if (stressLevel > Global::STRESS_FOR_MAX_RED_FACE)
            stressLevel = Global::STRESS_FOR_MAX_RED_FACE;

        int redness = static_cast<int>(255.0f * (Global::STRESS_FOR_MAX_RED_FACE - stressLevel) / Global::STRESS_FOR_MAX_RED_FACE);
        renderComp.GetSprite().SetOverlayColor(255, redness, redness);  
    }
}

//----------------------------------------------------------------------------
void StressLevelSystem::PostExecute()
{
    if (m_QuitTimer.getElapsedTime().asSeconds() > Global::CHECK_FOR_QUIT_TIMER)
    {
        m_QuitTimer.restart();
    }

    WorkerTotalStressUpdateEvent::Ptr stressEvent(new WorkerTotalStressUpdateEvent(m_TotalStressLevel));
    EventManager::GetInstance().Push(stressEvent);
}
