#pragma once

#include <forge/core/ecs/System.h>

#include <components/PhysicableComponent.h>

//----------------------------------------------------------------------------
class PhysicsSystem : public System<PhysicableComponent>
{
public:
    void Execute(const long long& _dt, Entity& _entity) const override;

private:
    static constexpr float k_Gravity = 9.807f;
    static constexpr int k_UnitPerMeter = 64;
};
