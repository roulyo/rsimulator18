#include <systems/PlayerControlSystem.h>

#include <events/inputs/MoveEvent.h>

#include <data/DataNameIds.h>

#include <cmath>

//----------------------------------------------------------------------------
PlayerControlSystem::PlayerControlSystem()
    : m_SpeedX(0.0f)
    , m_SpeedY(0.0f)
{ 
}

//----------------------------------------------------------------------------
PlayerControlSystem::~PlayerControlSystem()
{
}

//----------------------------------------------------------------------------
void PlayerControlSystem::Start()
{
    MoveEvent::Handlers += MoveEvent::Handler(this, &PlayerControlSystem::OnMoveEvent);
}

//----------------------------------------------------------------------------
void PlayerControlSystem::Stop()
{
    MoveEvent::Handlers -= MoveEvent::Handler(this, &PlayerControlSystem::OnMoveEvent);
}

//----------------------------------------------------------------------------
void PlayerControlSystem::Execute(const long long& _dt, Entity& _entity) const
{
    PhysicableComponent& physicComp = _entity.GetComponent<PhysicableComponent>();
    physicComp.SpeedX = m_SpeedX;
    physicComp.SpeedY = m_SpeedY;

    RenderableComponent& renderComp = _entity.GetComponent<RenderableComponent>();
}

//----------------------------------------------------------------------------
void PlayerControlSystem::OnMoveEvent(const MoveEvent& _event)
{
    MoveDirection direction = _event.GetDirection();

    switch (direction)
    {
    case MoveDirection::North:
        m_SpeedY = _event.IsMoving() ? -k_DefaultPlayerSpeed : 0.0f;
        break;
    case MoveDirection::East:
        m_SpeedX = _event.IsMoving() ? k_DefaultPlayerSpeed : 0.0f;
        break;
    case MoveDirection::South:
        m_SpeedY = _event.IsMoving() ? k_DefaultPlayerSpeed : 0.0f;
        break;
    case MoveDirection::West:
        m_SpeedX = _event.IsMoving() ? -k_DefaultPlayerSpeed : 0.0f;
        break;
    }

    NormalizeSpeed();
}

//----------------------------------------------------------------------------
void PlayerControlSystem::NormalizeSpeed()
{
    if (m_SpeedX != 0.0f || m_SpeedY != 0.0f)
    {
        float length = sqrt((m_SpeedX * m_SpeedX) + (m_SpeedY * m_SpeedY));

        m_SpeedX /= length;
        m_SpeedX *= k_DefaultPlayerSpeed;

        m_SpeedY /= length;
        m_SpeedY *= k_DefaultPlayerSpeed;
    }
}
