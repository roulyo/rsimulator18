#pragma once

#include <SFML/System/Clock.hpp>
#include <forge/core/ecs/System.h>
#include <components/WorkerComponent.h>
#include <components/RenderableComponent.h>

class StressLevelSystem : public System<WorkerComponent, RenderableComponent>
{
public:
    void PreExecute() override;
    void Execute(const long long& _dt, Entity& _entity) const override;
    void PostExecute() override;

private:
    sf::Clock       m_QuitTimer;
    mutable float   m_TotalStressLevel;
};