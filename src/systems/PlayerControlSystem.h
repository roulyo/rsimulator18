#pragma once

#include <forge/core/ecs/System.h>

#include <components/PlayableCharacterComponent.h>
#include <components/RenderableComponent.h>
#include <components/PhysicableComponent.h>

//----------------------------------------------------------------------------
class MoveEvent;

//----------------------------------------------------------------------------
class PlayerControlSystem : public System<PlayableCharacterComponent, RenderableComponent, PhysicableComponent>
{
public:
    PlayerControlSystem();
    ~PlayerControlSystem();

    void Start() override;
    void Stop() override;

    void Execute(const long long& _dt, Entity& _entity) const override;
    
    void OnMoveEvent(const MoveEvent& _event);

private:
    void NormalizeSpeed();

private:
    static constexpr float k_DefaultPlayerSpeed = 2.0f;

    float   m_SpeedX;
    float   m_SpeedY;
};
