#include <systems/RenderingSystem.h>

#include <forge/system/window/WindowManager.h>

#include <forge/2d/camera/CameraManager.h>

//----------------------------------------------------------------------------
void RenderingSystem::Execute(const long long& _dt, Entity& _entity) const
{
    RenderableComponent& renderComp = _entity.GetComponent<RenderableComponent>();
    Sprite& sprite = renderComp.GetSprite();
    
    float currentX = _entity.GetAABB().left;
    float currentY = _entity.GetAABB().top;

    float screenX = 0;
    float screenY = 0;

    CameraManager::GetInstance().GetMainCamera().WorldToViewport(currentX, currentY, screenX, screenY);

    sprite.SetScreenCoord(screenX, screenY);

    WindowManager::GetInstance().PushToRender(sprite.GetRenderingData());
}
