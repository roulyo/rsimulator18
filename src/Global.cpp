#include <Global.h>
#include <chrono>
#include <iostream>

//----------------------------------------------------------------------------
const int Global::NB_STARTING_WORKERS = 18;
const int Global::MAX_WORKER_SKILL_SCORE = 10;
const float Global::GAME_DURATION = 60.0f;
const float Global::AVAILABLE_MONEY = 120000.0f;
const float Global::WORKER_SALARY = 100.0f;
const float Global::STARTING_STRESS_LEVEL = 0.0f;
const float Global::MONEY_BONUS_BY_PATPAT = 1000.0f;

const int Global::STRESS_UP_BY_WHIP = 20;
const int Global::STRESS_DOWN_BY_PATPAT = 10;

const float Global::STATS_UP_BY_WHIP = 1.0f;
const float Global::STATS_UP_BY_PATPAT = 0.25f;

const float Global::PATPAT_STAT_BOOST_DURATION = 10.0f;
const float Global::WHIPPED_STAT_BOOST_DURATION = 5.0f;
const float Global::STRESS_LOST_BY_SECOND = 1.0f;
const float Global::CHECK_FOR_QUIT_TIMER = 3.0f;

const float Global::STRESS_FOR_MAX_RED_FACE = 150.0f;
const float Global::MIN_FEATURE_COST = 1500.0f;
const float Global::MAX_FEATURE_COST = 2500.0f;

//----------------------------------------------------------------------------
std::string Global::FeatureTypeToString(Skill type)
{
    int randomBuzzWord = rand() % 13;
    int randomFeatures = rand() % 9;
    switch (type)
    {
    case Skill::Art: return Global::buzzWords[randomBuzzWord] + Global::artFeatures[randomFeatures];
    case Skill::Code: return Global::buzzWords[randomBuzzWord] + Global::codeFeatures[randomFeatures];
    case Skill::Design: return Global::buzzWords[randomBuzzWord] + Global::designFeatures[randomFeatures];
    }

    return "Unknown feature type";
}

//----------------------------------------------------------------------------
void Global::ResetRandomSeed()
{
    auto now_ms = std::chrono::time_point_cast<std::chrono::microseconds>(std::chrono::system_clock::now());
    srand(static_cast<unsigned int>(now_ms.time_since_epoch().count()));
}

//----------------------------------------------------------------------------
std::vector < Global::position > Global::workers = { worker1, worker2, worker3, worker4, worker5, worker6 };

std::vector <std::string>  Global::artFeatures = { "animations", "chara design", "GUI", "mocap", "cinematics", "textures", "lighting", "concept art", "marketing assets"};
std::vector <std::string>  Global::codeFeatures = { "DevOps", "physics", "bug fixing", "servers", "matchmaking", "multi-player", "AI", "algorithm", "Data-Science"};
std::vector <std::string>  Global::designFeatures = { "retention", "monetization", "balancing", "level design", "world design", "season pass", "localization", "story-telling", "micro transactions", "QTE" };

std::vector <std::string>  Global::buzzWords = { "Systemic ", "Generic ", "Cheap ", "Seamless ", "Money milking ", "User friendly ", "Authentic ", "Award-Wining ", "Engaging ", "Immersive ", "Gritty ", "Remastered ", "Visceral ", "Contextual", "Disruptive" };

std::vector <std::string>  Global::startGameName = { "Struggle of the ", "Knights of the ", "Tales of the ", "Mortal ","Attack of ", "Future ", "World Of ", "Save Yourself from ", "Fartz Lords Presents : "};
std::vector <std::string>  Global::endGameName = { "Curse ", "Violation ", "Carnage ", "Solitude ", "Time ", "Machine ", "Crystal ", "Fantasy ", "Sushi Jihad "};
std::vector <std::string>  Global::followGameName = { "HD Remaster", "IV", "1.5.7", "1.2 Remix", "- 2",  " - The Revenge", "Dance Mix", "Pickle Edition", "Reloaded", "Deluxe", "Quizz"};