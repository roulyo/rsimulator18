#pragma once

#include <forge/system/event/Event.h>

//----------------------------------------------------------------------------
class WorkerQuitEvent : public Event<WorkerQuitEvent>
{
    forge_DeclEvent(WorkerQuitEvent);
};
