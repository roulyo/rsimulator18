#include <events/game/WorkersJobDoneEvent.h>

//----------------------------------------------------------------------------
WorkersJobDoneEvent::WorkersJobDoneEvent(Skill _skill, float _totalValue)
    : m_Skill(_skill)
    , m_TotalValue(_totalValue)
{}

//----------------------------------------------------------------------------
Skill WorkersJobDoneEvent::GetSkill() const
{
    return m_Skill;
}

//----------------------------------------------------------------------------
float WorkersJobDoneEvent::GetTotalValue() const
{
    return m_TotalValue;
}
