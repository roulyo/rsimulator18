#include <events/game/WorkerTotalStressUpdateEvent.h>

//----------------------------------------------------------------------------
WorkerTotalStressUpdateEvent::WorkerTotalStressUpdateEvent(float _totalValue)
    : m_TotalValue(_totalValue)
{}

//----------------------------------------------------------------------------
float WorkerTotalStressUpdateEvent::GetTotalValue() const
{
    return m_TotalValue;
}
