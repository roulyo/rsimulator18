#pragma once

#include <forge/system/event/Event.h>
#include <Global.h>


//----------------------------------------------------------------------------
class WorkersJobDoneEvent : public Event<WorkersJobDoneEvent>
{
    forge_DeclEvent(WorkersJobDoneEvent);

public:
    WorkersJobDoneEvent(Skill skill, float value);

    Skill GetSkill() const;
    float GetTotalValue() const;

private:
    Skill   m_Skill;
    float   m_TotalValue;

};
