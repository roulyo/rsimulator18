#pragma once

#include <forge/system/event/Event.h>
#include <Global.h>


//----------------------------------------------------------------------------
class WorkerTotalStressUpdateEvent : public Event<WorkerTotalStressUpdateEvent>
{
    forge_DeclEvent(WorkerTotalStressUpdateEvent);

public:
    WorkerTotalStressUpdateEvent(float value);

    float GetTotalValue() const;

private:
    float   m_TotalValue;
};
