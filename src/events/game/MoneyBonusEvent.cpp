#include <events/game/MoneyBonusEvent.h>

//----------------------------------------------------------------------------
MoneyBonusEvent::MoneyBonusEvent(float _bonus)
    : m_Bonus(_bonus)
{}


//----------------------------------------------------------------------------
float MoneyBonusEvent::GetBonus() const
{
    return m_Bonus;
}
