#pragma once

#include <forge/system/event/Event.h>
#include <Global.h>


//----------------------------------------------------------------------------
class MoneyBonusEvent : public Event<MoneyBonusEvent>
{
    forge_DeclEvent(MoneyBonusEvent);

public:
    MoneyBonusEvent(float _bonus);

    float GetBonus() const;

private:
    float   m_Bonus;

};
