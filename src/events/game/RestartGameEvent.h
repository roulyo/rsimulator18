#pragma once

#include <forge/system/event/Event.h>


//----------------------------------------------------------------------------
class RestartGameEvent : public Event<RestartGameEvent>
{
    forge_DeclEvent(RestartGameEvent);
};
