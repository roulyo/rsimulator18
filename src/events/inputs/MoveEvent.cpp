#include <events/inputs/MoveEvent.h>

//----------------------------------------------------------------------------
MoveEvent::MoveEvent(bool _isMoving, MoveDirection _direction)
    : m_IsMoving(_isMoving)
    , m_Direction(_direction)
{}

//----------------------------------------------------------------------------
bool MoveEvent::IsMoving() const
{
    return m_IsMoving;
}

//----------------------------------------------------------------------------
MoveDirection MoveEvent::GetDirection() const
{
    return m_Direction;
}
