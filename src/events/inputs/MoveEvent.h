#pragma once

#include <forge/system/event/Event.h>

//----------------------------------------------------------------------------
enum class MoveDirection
{
    North,
    East,
    South,
    West
};

//----------------------------------------------------------------------------
class MoveEvent : public Event<MoveEvent>
{
    forge_DeclEvent(MoveEvent);

public:
    MoveEvent(bool _isMoving, MoveDirection _direction);

    bool IsMoving() const;
    MoveDirection GetDirection() const;

private:
    bool            m_IsMoving;
    MoveDirection   m_Direction;

};

//----------------------------------------------------------------------------
template<bool _isMoving, MoveDirection _direction>
class OrientedMoveEvent : public MoveEvent
{
public:
    OrientedMoveEvent();

};

#include "MoveEvent.inl"
