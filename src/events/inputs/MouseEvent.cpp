#include <events/inputs/MouseEvent.h>

//----------------------------------------------------------------------------
MouseClickEvent::MouseClickEvent(bool _isPressed,unsigned short _x, unsigned short _y, MouseButton _button)
    : m_IsPressed(_isPressed)
    , m_X(_x)
    , m_Y(_y)
    , m_MouseButton(_button)
{}

//----------------------------------------------------------------------------
bool MouseClickEvent::GetIsPressed() const
{
    return m_IsPressed;
}

//----------------------------------------------------------------------------
unsigned short MouseClickEvent::GetX() const
{
    return m_X;
}

//----------------------------------------------------------------------------
unsigned short MouseClickEvent::GetY() const
{
    return m_Y;
}

//----------------------------------------------------------------------------
MouseButton MouseClickEvent::GetMouseButton() const
{
    return m_MouseButton;
}
